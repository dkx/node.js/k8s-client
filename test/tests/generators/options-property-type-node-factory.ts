import test from 'ava';
import * as ts from 'typescript';

import {Document} from '../../../src/openapi';
import {createOptionsPropertyTypeNode} from '../../../src/generators';


test('createOptionsPropertyTypeNode() throws an error for unsupported type', (t) => {

	t.throws(() => {
		createOptionsPropertyTypeNode(new Document({}), {
			type: 'json',
		});
	}, 'Definition property type "json" is not supported');
});

// primitive/boolean

test('createOptionsPropertyTypeNode() throws an error if boolean type has a format', (t) => {
	t.throws(() => {
		createOptionsPropertyTypeNode(new Document({}), {
			type: 'boolean',
			format: 'int32',
		});
	}, 'Definition property type "boolean" contains unexpected format "int32", but it should not have any');
});

test('createOptionsPropertyTypeNode() creates type for boolean', (t) => {
	const type = createOptionsPropertyTypeNode(new Document({}), {
		type: 'boolean',
	});

	t.is(type.typeNode.kind, ts.SyntaxKind.BooleanKeyword);
});

// primitive/string

test('createOptionsPropertyTypeNode() throws an error if string type has an unsupported format', (t) => {
	t.throws(() => {
		createOptionsPropertyTypeNode(new Document({}), {
			type: 'string',
			format: 'int32',
		});
	}, 'Definition property type "string" contains unexpected format "int32", it should be "byte" or none');
});

test('createOptionsPropertyTypeNode() creates type for string', (t) => {
	const type = createOptionsPropertyTypeNode(new Document({}), {
		type: 'string',
	});

	t.is(type.imports.length, 0);
	t.is(type.typeNode.kind, ts.SyntaxKind.StringKeyword);
});

test('createOptionsPropertyTypeNode() creates type for byte', (t) => {
	const type = createOptionsPropertyTypeNode(new Document({}), {
		type: 'string',
		format: 'byte',
	});

	t.is(type.imports.length, 0);
	t.is(type.typeNode.kind, ts.SyntaxKind.StringKeyword);
});

// primitive/number

test('createOptionsPropertyTypeNode() throws an error if format for number type is missing', (t) => {
	t.throws(() => {
		createOptionsPropertyTypeNode(new Document({}), {
			type: 'number',
		});
	}, 'Definition property type "number" does not contain a required format, it should be "double"');
});

test('createOptionsPropertyTypeNode() throws an error if number has invalid format', (t) => {
	t.throws(() => {
		createOptionsPropertyTypeNode(new Document({}), {
			type: 'number',
			format: 'byte',
		});
	}, 'Definition property type "number" contains unexpected format "byte", it should be "double"');
});

test('createOptionsPropertyTypeNode() creates type for double', (t) => {
	const type = createOptionsPropertyTypeNode(new Document({}), {
		type: 'number',
		format: 'double',
	});

	t.is(type.imports.length, 0);
	t.is(type.typeNode.kind, ts.SyntaxKind.NumberKeyword);
});

// primitive/integer

test('createOptionsPropertyTypeNode() throws an error if format for integer type is missing', (t) => {
	t.throws(() => {
		createOptionsPropertyTypeNode(new Document({}), {
			type: 'integer',
		});
	}, 'Definition property type "integer" does not contain a required format, it should be "int32", "int64"');
});

test('createOptionsPropertyTypeNode() throws an error if integer has invalid format', (t) => {
	t.throws(() => {
		createOptionsPropertyTypeNode(new Document({}), {
			type: 'integer',
			format: 'byte',
		});
	}, 'Definition property type "integer" contains unexpected format "byte", it should be "int32", "int64"');
});

test('createOptionsPropertyTypeNode() creates type for int32', (t) => {
	const type = createOptionsPropertyTypeNode(new Document({}), {
		type: 'integer',
		format: 'int32',
	});

	t.is(type.imports.length, 0);
	t.is(type.typeNode.kind, ts.SyntaxKind.NumberKeyword);
});

test('createOptionsPropertyTypeNode() creates type for int64', (t) => {
	const type = createOptionsPropertyTypeNode(new Document({}), {
		type: 'integer',
		format: 'int64',
	});

	t.is(type.imports.length, 0);
	t.is(type.typeNode.kind, ts.SyntaxKind.NumberKeyword);
});

// reference

test('createOptionsPropertyTypeNode() creates referenced type', (t) => {
	const data = createOptionsPropertyTypeNode(new Document({
		definitions: {
			ReferencedResource: {},
		},
	}), {
		$ref: '#/definitions/ReferencedResource',
	});

	t.is(data.imports.length, 1);
	t.is(data.imports[0].targetFileName, 'resource/referenced-resource.ts');
	t.deepEqual(data.imports[0].names, ['ReferencedResource', 'ReferencedResourceOptions']);
	t.true(ts.isUnionTypeNode(data.typeNode));
	t.is((<ts.UnionTypeNode>data.typeNode).types.length, 2);
	t.true(ts.isTypeReferenceNode((<ts.UnionTypeNode>data.typeNode).types[0]));
	t.true(ts.isTypeReferenceNode((<ts.UnionTypeNode>data.typeNode).types[1]));
	t.true(ts.isIdentifier((<ts.TypeReferenceNode>((<ts.UnionTypeNode>data.typeNode).types[0])).typeName));
	t.true(ts.isIdentifier((<ts.TypeReferenceNode>((<ts.UnionTypeNode>data.typeNode).types[1])).typeName));
	t.is((<ts.Identifier>((<ts.TypeReferenceNode>((<ts.UnionTypeNode>data.typeNode).types[0])).typeName)).text, 'ReferencedResource');
	t.is((<ts.Identifier>((<ts.TypeReferenceNode>((<ts.UnionTypeNode>data.typeNode).types[1])).typeName)).text, 'ReferencedResourceOptions');
});
