import test from 'ava';

import {CodeWriter, DefinitionGenerator} from '../../../src/generators';
import {Definition, Document} from '../../../src/openapi';


test('generateDefinition() create simple definition', (t) => {
	const gen = new DefinitionGenerator(new CodeWriter());
	const def = new Definition(new Document({
		definitions: {
			ReferencedResource: {},
		},
	}), 'TestResource', {
		properties: {
			primitiveOptional: {
				type: 'string',
			},
			primitiveRequired: {
				type: 'string',
			},
			referenceOptional: {
				$ref: '#/definitions/ReferencedResource',
			},
			referenceRequired: {
				$ref: '#/definitions/ReferencedResource',
			},
		},
		required: ['primitiveRequired', 'referenceRequired'],
	});

	const code = gen.generateDefinition(def);

	t.is(code, `import { ReferencedResource, ReferencedResourceOptions } from "./referenced-resource";
export declare interface TestResourceOptions {
    "primitiveRequired": string;
    "referenceRequired": ReferencedResource | ReferencedResourceOptions;
    "primitiveOptional"?: string;
    "referenceOptional"?: ReferencedResource | ReferencedResourceOptions;
}
`);
});
