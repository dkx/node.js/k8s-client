import test from 'ava';
import * as ts from 'typescript';
import * as path from 'path';
import * as fs from 'fs-extra';

import {CodeWriter} from '../../../src/generators';


test('write() prints code', (t) => {
	const writer = new CodeWriter();
	const code = writer.write('tmp.ts', [
		ts.createExpressionStatement(ts.createTrue()),
	]);

	t.is(code.trim(), 'true;');
});

test('writeToFile() prints code', async (t) => {
	const writer = new CodeWriter();
	const file = path.join(__dirname, 'write-to-file.ts');

	await writer.writeToFile(file, [
		ts.createExpressionStatement(ts.createTrue()),
	]);

	t.is(await fs.pathExists(file), true);
	t.is((await fs.readFile(file, {encoding: 'utf8'})).trim(), 'true;');

	await fs.unlink(file);
});
