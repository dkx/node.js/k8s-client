import test from 'ava';

import {Document} from '../../../src/openapi';


test('definitions contain no definition', (t) => {
	const doc = new Document({});

	t.deepEqual(doc.definitions, []);
});

test('definitions contain simple definition', (t) => {
	const doc = new Document({
		definitions: {
			'io.k8s.api.core.v1.Namespace': {},
		},
	});

	t.is(doc.definitions.length, 1);
	t.is(doc.definitions[0].name, 'io.k8s.api.core.v1.Namespace');
});

test('getDefinitionByName() throws an error on unknown definition', (t) => {
	const doc = new Document({});

	t.throws(() => {
		doc.getDefinitionByName('unknown');
	}, 'Document: definition "unknown" does not exists');
});

test('getDefinitionByName() returns a definition', (t) => {
	const doc = new Document({
		definitions: {
			'io.k8s.api.core.v1.Namespace': {},
		},
	});

	const found = doc.getDefinitionByName('io.k8s.api.core.v1.Namespace');

	t.is(found.name, 'io.k8s.api.core.v1.Namespace');
	t.is(found, doc.definitions[0]);
});
