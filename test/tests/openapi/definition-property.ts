import test from 'ava';

import {Document, DefinitionProperty} from '../../../src/openapi';


test('description is empty', (t) => {
	const def = new DefinitionProperty(new Document({}), 'apiVersion', false, {});

	t.is(typeof def.description, 'undefined');
});

test('description is not empty', (t) => {
	const def = new DefinitionProperty(new Document({}), 'apiVersion', false, {
		description: 'APIVersion defines the versioned schema',
	});

	t.is(def.description, 'APIVersion defines the versioned schema');
});
