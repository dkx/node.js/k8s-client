import test from 'ava';

import {Definition, Document} from '../../../src/openapi';


test('description is be empty', (t) => {
	const def = new Definition(new Document({}), 'io.k8s.api.core.v1.Namespace', {});

	t.is(typeof def.description, 'undefined');
});

test('description is not be empty', (t) => {
	const description = 'Namespace provides a scope for Names. Use of multiple namespaces is optional.';
	const def = new Definition(new Document({}), 'io.k8s.api.core.v1.Namespace', {description});

	t.is(def.description, description);
});

test('properties is empty', (t) => {
	const def = new Definition(new Document({}), 'io.k8s.api.core.v1.Namespace', {});

	t.is(def.properties.length, 0);
});

test('properties contains optional property', (t) => {
	const def = new Definition(new Document({}), 'io.k8s.api.core.v1.Namespace', {
		properties: {
			apiVersion: {
				type: 'string',
			},
		},
	});

	t.is(def.properties.length, 1);
	t.is(def.properties[0].required, false);
});

test('properties contains required property', (t) => {
	const def = new Definition(new Document({}), 'io.k8s.api.core.v1.Namespace', {
		properties: {
			apiVersion: {
				type: 'string',
			},
		},
		required: ['apiVersion'],
	});

	t.is(def.properties.length, 1);
	t.is(def.properties[0].required, true);
});

test('fileName contain relative file path', (t) => {
	const def = new Definition(new Document({}), 'io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta', {});

	t.is(def.fileName, 'resource/io/k8s/apimachinery/pkg/apis/meta/v1/object-meta.ts');
});

test('className contains the definition class name', (t) => {
	const def = new Definition(new Document({}), 'io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta', {});

	t.is(def.className, 'ObjectMeta');
});

test('optionsClassName contains the options class name', (t) => {
	const def = new Definition(new Document({}), 'io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta', {});

	t.is(def.optionsClassName, 'ObjectMetaOptions');
});
