import test from 'ava';
import {relativeImportPath} from '../../../src/utils';


test('relativeImportPath() creates relative path for files in same directories', (t) => {
	t.is(relativeImportPath('a.ts', 'b.ts'), './b');
});

test('relativeImportPath() creates relative path to an index file', (t) => {
	t.is(relativeImportPath('a.ts', 'b/index.ts'), './b');
});
