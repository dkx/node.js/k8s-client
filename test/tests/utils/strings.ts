import test from 'ava';

import {camelCaseToHyphens, firstLower, insertString} from '../../../src/utils';


test('firstLower() does nothing on lower case string', (t) => {
	t.is(firstLower('hello'), 'hello');
});

test('firstLower() updates the first letter', (t) => {
	t.is(firstLower('HELLO'), 'hELLO');
});

test('insertString() insert string into another string', (t) => {
	t.is(insertString('helloworld', 5, ' '), 'hello world');
});

test('camelCaseToHyphens() does nothing', (t) => {
	t.is(camelCaseToHyphens('hello-world'), 'hello-world');
});

test('camelCaseToHyphens() transform to dashes', (t) => {
	t.is(camelCaseToHyphens('HelloWorld'), 'hello-world');
});

test('camelCaseToHyphens() correctly transforms multiple uppercased letters in a row', (t) => {
	t.is(camelCaseToHyphens('ServerAddressByClientCIDR'), 'server-address-by-client-cidr');
});

test('camelCaseToHyphens() correctly transforms multiple uppercased letters at the beginning in a row', (t) => {
	t.is(camelCaseToHyphens('APIGroup'), 'api-group');
});
