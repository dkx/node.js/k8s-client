import * as ts from 'typescript';
import * as path from 'path';

import {Definition, DefinitionProperty} from '../openapi';
import {CodeWriter} from './code-writer';
import {
	addJSDoc,
	createNamedImportsFromRequests,
	createNodeWithImportRequests,
	ImportRequest,
	NodeWithImportRequests
} from '../utils';


export class DefinitionGenerator
{
	constructor(
		private readonly writer: CodeWriter,
		private readonly outputDir: string,
	) {}

	public async generateDefinition(definition: Definition): Promise<void>
	{
		const statements: Array<ts.Statement> = [];
		const resource = this.generateClass(definition);
		let imports: Array<ImportRequest> = resource.imports;

		if (!definition.isSimple()) {
			const options = this.generateOptions(definition);
			imports = [...imports, ...options.imports];
			statements.push(options.node);
		}

		statements.push(resource.node);

		await this.writer.writeToFile(path.join(this.outputDir, definition.fileName), [
			...createNamedImportsFromRequests(definition.fileName, imports),
			...statements,
		]);
	}

	/**
	 * Generates:
	 * export declare interface <ResourceName>Options { <DefinitionProperties> }
	 */
	private generateOptions(definition: Definition): NodeWithImportRequests<ts.InterfaceDeclaration>
	{
		let imports: Array<ImportRequest> = [];
		const options = ts.createInterfaceDeclaration(
			[],
			[
				ts.createToken(ts.SyntaxKind.ExportKeyword),
				ts.createToken(ts.SyntaxKind.DeclareKeyword),
			],
			definition.optionsClassName,
			[],
			[],
			[
				...definition.properties.map((property) => {
					const data = this.generateOptionsProperty(property);
					imports = [...imports, ...data.imports];
					return data.node;
				}),
			],
		);

		const comments: Array<string> = [
			definition.name,
		];

		if (typeof definition.description !== 'undefined' && definition.description !== '') {
			comments.push(definition.description);
		}

		return createNodeWithImportRequests(addJSDoc(options, comments), imports);
	}

	/**
	 * Generates:
	 * "<propertyName>"<questionToken>: <propertyType>
	 */
	private generateOptionsProperty(property: DefinitionProperty): NodeWithImportRequests<ts.PropertySignature>
	{
		const typeInfo = property.createTypeNode(true);
		let node = ts.createPropertySignature(
			[],
			ts.createStringLiteral(property.name),
			property.required ? undefined : ts.createToken(ts.SyntaxKind.QuestionToken),
			typeInfo.node,
			undefined,
		);

		if (typeof property.description !== 'undefined' && property.description !== '') {
			node = addJSDoc(node, [
				property.description,
			]);
		}

		return createNodeWithImportRequests(node, typeInfo.imports);
	}

	/**
	 * Generates:
	 * export class <ResourceClass> implements Resource { ... }
	 */
	private generateClass(definition: Definition): NodeWithImportRequests<ts.ClassDeclaration>
	{
		const ctor = this.generateConstructor(definition);
		const fromJsonFactory = this.generateFromJsonFactory(definition);

		let imports: Array<ImportRequest> = [
			{
				targetFileName: 'resource/resource-interface.ts',
				names: ['Resource'],
			},
			...ctor.imports,
			...fromJsonFactory.imports,
		];

		let resource = ts.createClassDeclaration(
			[],
			[
				ts.createToken(ts.SyntaxKind.ExportKeyword),
			],
			definition.className,
			[],
			[
				ts.createHeritageClause(ts.SyntaxKind.ImplementsKeyword, [
					ts.createExpressionWithTypeArguments([], ts.createIdentifier('Resource')),
				]),
			],
			[
				...definition.properties.map((property) => {
					const data = this.generateClassProperty(property);
					imports = [...imports, ...data.imports];
					return data.node;
				}),
				ctor.node,
				fromJsonFactory.node,
				this.generateToJsonMethod(definition),
			],
		);

		const comments: Array<string> = [
			definition.name,
		];

		if (typeof definition.description !== 'undefined' && definition.description !== '') {
			comments.push(definition.description);
		}

		return createNodeWithImportRequests(addJSDoc(resource, comments), imports);
	}


	/**
	 * Generates:
	 * public readonly "<propertyName>"<questionToken>: <propertyType>
	 */
	private generateClassProperty(property: DefinitionProperty): NodeWithImportRequests<ts.PropertyDeclaration>
	{
		const typeInfo = property.createTypeNode(false);
		let node = ts.createProperty(
			[],
			[
				ts.createToken(ts.SyntaxKind.PublicKeyword),
				ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
			],
			ts.createStringLiteral(property.name),
			property.required ? undefined : ts.createToken(ts.SyntaxKind.QuestionToken),
			typeInfo.node,
			undefined,
		);

		if (typeof property.description !== 'undefined' && property.description !== '') {
			node = addJSDoc(node, [
				property.description,
			]);
		}

		return createNodeWithImportRequests(node, typeInfo.imports);
	}

	/**
	 * Generates:
	 * public constructor(options: <ResourceOptionClass> = {}) { ... }
	 */
	private generateConstructor(definition: Definition): NodeWithImportRequests<ts.ConstructorDeclaration>
	{
		let imports: Array<ImportRequest> = [];
		const parameters: Array<ts.ParameterDeclaration> = [];

		if (definition.isSimple()) {
			parameters.push(ts.createParameter(
				[],
				[
					ts.createToken(ts.SyntaxKind.PublicKeyword),
					ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
				],
				undefined,
				'data',
				undefined,
				ts.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword),
				ts.createObjectLiteral([], false),
			));
		} else {
			parameters.push(ts.createParameter(
				[],
				[],
				undefined,
				'options',
				undefined,
				ts.createTypeReferenceNode(ts.createIdentifier(definition.optionsClassName), []),
				definition.hasRequiredProperty() ? undefined : ts.createObjectLiteral([], false),
			));
		}

		const node = ts.createConstructor(
			[],
			[
				ts.createToken(ts.SyntaxKind.PublicKeyword),
			],
			parameters,
			ts.createBlock([
				...definition.properties.map((property) => {
					const data = this.generateOptionAssignment(property);
					imports = [...imports, ...data.imports];
					return data.node;
				}),
			], true),
		);

		return createNodeWithImportRequests(node, imports);
	}

	/**
	 * Generates:
	 * if (typeof options["<optionPropertyName>"] !== "undefined") {
	 *     this["<optionPropertyName>"] = <optionAssignment>;
	 * }
	 */
	private generateOptionAssignment(property: DefinitionProperty): NodeWithImportRequests<ts.Statement>
	{
		const option = ts.createElementAccess(ts.createIdentifier('options'), ts.createStringLiteral(property.name));
		const accessor = property.createOptionsGetterNode(option);

		let node: ts.Statement = ts.createExpressionStatement(
			ts.createBinary(
				ts.createElementAccess(ts.createThis(), ts.createStringLiteral(property.name)),
				ts.createToken(ts.SyntaxKind.EqualsToken),
				accessor.node,
			),
		);

		if (!property.required) {
			node = ts.createIf(
				ts.createBinary(
					ts.createTypeOf(
						option,
					),
					ts.createToken(ts.SyntaxKind.ExclamationEqualsEqualsToken),
					ts.createStringLiteral('undefined'),
				),
				ts.createBlock([
					node,
				], true),
				undefined,
			);
		}

		return createNodeWithImportRequests(node, accessor.imports);
	}

	/**
	 * Generates:
	 * public static createFromJson(data: any): <ResourceName>
	 * {
	 * 		...
	 * 		const options: <ResourceName>Options = { ... }
	 * 		...
	 * 		return new <ResourceName>(options);
	 * }
	 */
	private generateFromJsonFactory(definition: Definition): NodeWithImportRequests<ts.MethodDeclaration>
	{
		let imports: Array<ImportRequest> = [];
		let statements: Array<ts.Statement> = [];

		if (definition.isSimple()) {
			statements.push(ts.createReturn(
				ts.createNew(
					ts.createIdentifier(definition.className),
					[],
					[
						ts.createIdentifier('data'),
					],
				),
			));
		} else {
			statements = [
				...definition.properties
					.filter((property) => property.required)
					.map((property) => this.generateFromJsonPropertyAssertion(definition, property)),
				ts.createVariableStatement([], ts.createVariableDeclarationList([
					ts.createVariableDeclaration(
						'options',
						ts.createTypeReferenceNode(definition.optionsClassName, []),
						ts.createObjectLiteral(
							definition.properties
								.filter((property) => property.required)
								.map((property) => {
									const data = this.generateFromJsonRequiredPropertyAssignment(property);
									imports = [...imports, ...data.imports];
									return data.node;
								}),
							definition.hasRequiredProperty(),
						),
					),
				], ts.NodeFlags.Const)),
				...definition.properties
					.filter((property) => !property.required)
					.map((property) => {
						const data = this.generateFromJsonOptionalPropertyAssignment(property);
						imports = [...imports, ...data.imports];
						return data.node;
					}),
				ts.createReturn(
					ts.createNew(
						ts.createIdentifier(definition.className),
						[],
						[
							ts.createIdentifier('options'),
						],
					),
				),
			];
		}

		const node = ts.createMethod(
			[],
			[
				ts.createToken(ts.SyntaxKind.PublicKeyword),
				ts.createToken(ts.SyntaxKind.StaticKeyword),
			],
			undefined,
			'createFromJson',
			undefined,
			[],
			[
				ts.createParameter(
					[],
					[],
					undefined,
					'data',
					undefined,
					ts.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword),
				),
			],
			ts.createTypeReferenceNode(definition.className, []),
			ts.createBlock(statements, true),
		);

		return createNodeWithImportRequests(node, imports);
	}

	/**
	 * Generates:
	 * if (typeof options["<propertyName>"] === 'undefined') {
	 * 		throw new Error('<ResourceName>.createFromJson: required property "<propertyName>" is missing');
	 * }
	 */
	private generateFromJsonPropertyAssertion(definition: Definition, property: DefinitionProperty): ts.IfStatement
	{
		return ts.createIf(
			ts.createBinary(
				ts.createTypeOf(
					ts.createElementAccess(ts.createIdentifier('data'), ts.createStringLiteral(property.name)),
				),
				ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
				ts.createStringLiteral('undefined'),
			),
			ts.createBlock([
				ts.createThrow(
					ts.createNew(
						ts.createIdentifier('Error'),
						[],
						[
							ts.createStringLiteral(`${definition.className}.createFromJson: required property "${property.name}" is missing`),
						],
					),
				),
			], true),
		);
	}

	/**
	 * Generates:
	 * "<propertyName>": <propertyAccessor>
	 */
	private generateFromJsonRequiredPropertyAssignment(property: DefinitionProperty): NodeWithImportRequests<ts.PropertyAssignment>
	{
		const nodeInfo = property.createFromJsonGetterNode(
			ts.createElementAccess(ts.createIdentifier('data'), ts.createStringLiteral(property.name)),
		);

		const node = ts.createPropertyAssignment(
			ts.createStringLiteral(property.name),
			nodeInfo.node,
		);

		return createNodeWithImportRequests(node, nodeInfo.imports);
	}

	/**
	 * Generates:
	 * if (typeof data["<propertyName>"] !== "undefined") {
	 * 		options["<propertyName>"] = <propertyAccessor>;
	 * }
	 */
	private generateFromJsonOptionalPropertyAssignment(property: DefinitionProperty): NodeWithImportRequests<ts.IfStatement>
	{
		const accessor = ts.createElementAccess(ts.createIdentifier('data'), ts.createStringLiteral(property.name));
		const nodeInfo = property.createFromJsonGetterNode(
			accessor,
		);

		const node = ts.createIf(
			ts.createBinary(
				ts.createTypeOf(accessor),
				ts.createToken(ts.SyntaxKind.ExclamationEqualsEqualsToken),
				ts.createStringLiteral('undefined'),
			),
			ts.createBlock([
				ts.createExpressionStatement(
					ts.createBinary(
						ts.createElementAccess(ts.createIdentifier('options'), ts.createStringLiteral(property.name)),
						ts.createToken(ts.SyntaxKind.EqualsToken),
						nodeInfo.node,
					),
				),
			], true),
		);

		return createNodeWithImportRequests(node, nodeInfo.imports);
	}

	/**
	 * Generates:
	 * public toJson(): any
	 * {
	 * 		const data: any = { ... };
	 * 		...
	 * 		return data;
	 * }
	 */
	private generateToJsonMethod(definition: Definition): ts.MethodDeclaration
	{
		let statements: Array<ts.Statement> = [];

		if (definition.isSimple()) {
			statements.push(ts.createReturn(
				ts.createPropertyAccess(ts.createThis(), 'data'),
			));
		} else {
			statements = [
				ts.createVariableStatement([], ts.createVariableDeclarationList([
					ts.createVariableDeclaration(
						'data',
						ts.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword),
						ts.createObjectLiteral(
							definition.properties
								.filter((property) => property.required)
								.map((property) => this.generateToJsonRequiredPropertyAssignment(property)),
							definition.hasRequiredProperty(),
						),
					),
				], ts.NodeFlags.Const)),
				...definition.properties
					.filter((property) => !property.required)
					.map((property) => this.generateToJsonOptionalPropertyAssignment(property)),
				ts.createReturn(
					ts.createIdentifier('data'),
				),
			];
		}

		return ts.createMethod(
			[],
			[
				ts.createToken(ts.SyntaxKind.PublicKeyword),
			],
			undefined,
			'toJson',
			undefined,
			[],
			[],
			ts.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword),
			ts.createBlock(statements, true),
		);
	}

	/**
	 * Generates:
	 * "<propertyName>": <propertyAccessor>
	 */
	private generateToJsonRequiredPropertyAssignment(property: DefinitionProperty): ts.PropertyAssignment
	{
		return ts.createPropertyAssignment(
			ts.createStringLiteral(property.name),
			property.createToJsonGetterNode(
				ts.createElementAccess(ts.createThis(), ts.createStringLiteral(property.name)),
			),
		);
	}

	/**
	 * Generates:
	 * if (typeof this["<propertyName>"] !== "undefined") {
	 * 		data["<propertyName>"] = this["<propertyName>"];
	 * }
	 */
	private generateToJsonOptionalPropertyAssignment(property: DefinitionProperty): ts.IfStatement
	{
		const accessor = ts.createElementAccess(ts.createThis(), ts.createStringLiteral(property.name));

		return ts.createIf(
			ts.createBinary(
				ts.createTypeOf(accessor),
				ts.createToken(ts.SyntaxKind.ExclamationEqualsEqualsToken),
				ts.createStringLiteral('undefined'),
			),
			ts.createBlock([
				ts.createExpressionStatement(
					ts.createBinary(
						ts.createElementAccess(ts.createIdentifier('data'), ts.createStringLiteral(property.name)),
						ts.createToken(ts.SyntaxKind.EqualsToken),
						property.createToJsonGetterNode(accessor),
					),
				),
			], false),
		);
	}
}
