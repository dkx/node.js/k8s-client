import * as ts from 'typescript';
import * as fs from 'fs-extra';
import * as path from 'path';


export class CodeWriter
{
	public write(fileName: string, statements: Array<ts.Statement>): string
	{
		const file = ts.createSourceFile(fileName, '', ts.ScriptTarget.Latest, false, ts.ScriptKind.TS);
		const printer = ts.createPrinter({
			newLine: ts.NewLineKind.LineFeed,
		});

		file.statements = ts.createNodeArray(statements);

		return printer.printNode(ts.EmitHint.Unspecified, file, file);
	}

	public async writeToFile(fileName: string, statements: Array<ts.Statement>): Promise<void>
	{
		const code = this.write(fileName, statements);
		await fs.mkdirp(path.dirname(fileName));
		await fs.writeFile(fileName, code, {encoding: 'utf8'});
	}
}
