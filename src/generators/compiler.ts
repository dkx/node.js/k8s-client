import * as readdir from 'recursive-readdir';
import * as ts from 'typescript';
import * as fs from 'fs-extra';


export class Compiler
{
	constructor(
		private readonly outputDir: string,
	) {}

	public async compileAll(): Promise<void>
	{
		const files = await readdir(this.outputDir);

		const program = ts.createProgram(files, {
			noEmitOnError: true,
			declaration: true,
			target: ts.ScriptTarget.ES5,
			module: ts.ModuleKind.CommonJS,
		});

		const emitResult = program.emit();
		const allDiagnostics = ts.getPreEmitDiagnostics(program).concat(emitResult.diagnostics);

		allDiagnostics.forEach((diagnostic) => {
			if (diagnostic.file) {
				const { line, character } = diagnostic.file.getLineAndCharacterOfPosition(diagnostic.start!);
				const message = ts.flattenDiagnosticMessageText(diagnostic.messageText, "\n");
				console.log(`${diagnostic.file.fileName} (${line + 1},${character + 1}): ${message}`);
			} else {
				console.log(ts.flattenDiagnosticMessageText(diagnostic.messageText, "\n"));
			}
		});

		if (emitResult.emitSkipped) {
			throw new Error('Failed to compile generated code');
		}

		for (let file of files) {
			await fs.remove(file);
		}
	}
}
