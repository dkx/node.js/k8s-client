import * as ts from 'typescript';
import * as path from 'path';

import {
	Definition,
	Endpoint,
	EndpointsGroup,
	EndpointsGroupFactory,
	OpenApiKubernetesAction,
	TypedObject
} from '../openapi';
import {ReferenceType} from '../openapi/types/reference-type';
import {RootEndpointGroup} from '../openapi/root-endpoint-group';
import {CodeWriter} from './code-writer';
import {
	addJSDoc,
	createNamedImportsFromRequests,
	createNodeWithImportRequests,
	firstUpper,
	ImportRequest,
	NodeWithImportRequests
} from '../utils';


export class EndpointsGroupsGenerator
{
	constructor(
		private readonly writer: CodeWriter,
		private readonly outputDir: string,
	) {}

	public async generate(group: EndpointsGroup): Promise<void>
	{
		const api = this.generateClass(group);

		const statements: Array<ts.Statement> = [
			...createNamedImportsFromRequests(group.fileName, [
				...api.imports,
			]),
			api.node,
		];

		await this.writer.writeToFile(path.join(this.outputDir, group.fileName), statements);
	}

	/**
	 * Generates:
	 * export class <EndpointGroupClass> { ... }
	 */
	private generateClass(group: EndpointsGroup): NodeWithImportRequests<ts.ClassDeclaration>
	{
		const ctor = this.generateConstructor(group);
		let imports: Array<ImportRequest> = ctor.imports;

		const members: Array<ts.ClassElement> = [
			ctor.node,
			...group.factories.map((factory) => {
				const data = this.generateFactoryMethod(group, factory);
				imports = [...imports, ...data.imports];
				return data.node;
			}),
			...group.endpoints.map((endpoint) => {
				const data = this.generateEndpoint(group, endpoint);
				imports = [...imports, ...data.imports];
				return data.node;
			}),
		];

		const patchResourceDefinition = group.getResourceForPatchResourceShortcut();
		if (typeof patchResourceDefinition !== 'undefined') {
			const patchResource = this.generateShortcutPatchResource(patchResourceDefinition);
			members.push(patchResource.node);
			imports = [...imports, ...patchResource.imports];
		}

		const postOrPutResourceDefinition = group.getResourceForPostOrShortcut(OpenApiKubernetesAction.Put);
		if (typeof postOrPutResourceDefinition !== 'undefined') {
			const postOrPutResource = this.generateShortcutPostOr(postOrPutResourceDefinition, 'put');
			members.push(postOrPutResource.node);
			imports = [...imports, ...postOrPutResource.imports];
		}

		const postOrPatchResourceDefinition = group.getResourceForPostOrShortcut(OpenApiKubernetesAction.Patch);
		if (typeof postOrPatchResourceDefinition !== 'undefined') {
			const postOrPatchResource = this.generateShortcutPostOr(postOrPatchResourceDefinition, 'patch');
			members.push(postOrPatchResource.node);
			imports = [...imports, ...postOrPatchResource.imports];
		}

		if (group.shouldGenerateExistsShortcut()) {
			const exists = this.generateShortcutExists();
			members.push(exists.node);
			imports = [...imports, ...exists.imports];
		}

		let node = ts.createClassDeclaration(
			[],
			[
				ts.createToken(ts.SyntaxKind.ExportKeyword),
			],
			group.className,
			[],
			[],
			members,
		);

		if (typeof group.url !== 'undefined') {
			node = addJSDoc(node, [
				group.url,
			]);
		}

		return createNodeWithImportRequests(node, imports);
	}

	/**
	 * Generates:
	 * public constructor(
	 * 		public readonly fetch: Fetch,
	 * 		public readonly logger: Logger,
	 * 		public readonly jsonPatchGenerator: JsonPatchGenerator,
	 * 		...
	 * ) {}
	 */
	private generateConstructor(group: EndpointsGroup): NodeWithImportRequests<ts.ConstructorDeclaration>
	{
		const isRoot = group instanceof RootEndpointGroup;
		const node = ts.createConstructor(
			[],
			[
				ts.createToken(ts.SyntaxKind.PublicKeyword),
			],
			[
				ts.createParameter(
					[],
					[
						ts.createToken(ts.SyntaxKind.PublicKeyword),
						ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
					],
					undefined,
					'fetch',
					undefined,
					ts.createTypeReferenceNode('Fetch', []),
				),
				ts.createParameter(
					[],
					[
						ts.createToken(ts.SyntaxKind.PublicKeyword),
						ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
					],
					undefined,
					'logger',
					undefined,
					ts.createTypeReferenceNode('Logger', []),
					isRoot ? ts.createNew(ts.createIdentifier('StdOutLogger'), [], []) : undefined,
				),
				ts.createParameter(
					[],
					[
						ts.createToken(ts.SyntaxKind.PublicKeyword),
						ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
					],
					undefined,
					'jsonPatchGenerator',
					undefined,
					ts.createTypeReferenceNode('JsonPatchGenerator', []),
					isRoot ? ts.createNew(ts.createIdentifier('JsonPatchGenerator'), [], []) : undefined,
				),
				...group.urlParameters.map((parameter) => {
					return ts.createParameter(
						[],
						[
							ts.createToken(ts.SyntaxKind.PrivateKeyword),
							ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
						],
						undefined,
						parameter.name,
						undefined,
						ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
					);
				}),
			],
			ts.createBlock([], false),
		);

		const loggingImportNames: Array<string> = ['Logger'];
		if (isRoot) {
			loggingImportNames.push('StdOutLogger');
		}

		return createNodeWithImportRequests(node, [
			{
				targetFileName: 'fetch.ts',
				names: ['Fetch'],
			},
			{
				targetFileName: 'logging/index.ts',
				names: loggingImportNames,
			},
			{
				targetFileName: 'json-patch-generator.ts',
				names: ['JsonPatchGenerator'],
			},
		]);
	}

	/**
	 * Generates:
	 * public get <factoryName>( ... ): <TargetEndpointClass> { ... }
	 */
	private generateFactoryMethod(parentGroup: EndpointsGroup, factory: EndpointsGroupFactory): NodeWithImportRequests<ts.ClassElement>
	{
		const returnNode = this.generateFactory(parentGroup, factory);
		const returnType = ts.createTypeReferenceNode(factory.targetGroup.className, []);
		const body = ts.createBlock([
			returnNode.node,
		], true);

		if (typeof factory.targetGroup.factoryParameter === 'undefined') {
			const node = ts.createGetAccessor(
				[],
				[
					ts.createToken(ts.SyntaxKind.PublicKeyword),
				],
				factory.methodName,
				[],
				returnType,
				body,
			);

			return createNodeWithImportRequests(node, returnNode.imports);
		}

		const node = ts.createMethod(
			[],
			[
				ts.createToken(ts.SyntaxKind.PublicKeyword),
			],
			undefined,
			factory.methodName,
			undefined,
			[],
			[
				ts.createParameter(
					[],
					[],
					undefined,
					factory.targetGroup.factoryParameter.name,
					undefined,
					ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
				),
			],
			returnType,
			body,
		);

		return createNodeWithImportRequests(node, returnNode.imports);
	}

	/**
	 * Generates:
	 * return new <TargetEndpointClass>(this.fetch, this.logger, this.jsonPatchGenerator, ...)
	 */
	private generateFactory(parentGroup: EndpointsGroup, factory: EndpointsGroupFactory): NodeWithImportRequests<ts.ReturnStatement>
	{
		const args: Array<ts.Expression> = [
			ts.createPropertyAccess(ts.createThis(), 'fetch'),
			ts.createPropertyAccess(ts.createThis(), 'logger'),
			ts.createPropertyAccess(ts.createThis(), 'jsonPatchGenerator'),
			...parentGroup.urlParameters.map((parameter) => {
				return ts.createPropertyAccess(ts.createThis(), parameter.name);
			}),
		];

		if (typeof factory.targetGroup.factoryParameter !== 'undefined') {
			args.push(ts.createIdentifier(factory.targetGroup.factoryParameter.name));
		}

		const node = ts.createReturn(
			ts.createNew(
				ts.createIdentifier(factory.targetGroup.className),
				[],
				args,
			),
		);

		return createNodeWithImportRequests(node, [
			{
				targetFileName: factory.targetGroup.fileName,
				names: [factory.targetGroup.className],
			},
		]);
	}

	/**
	 * Generates:
	 * public async <endpointMethod>( ... ): Promise<ResponseType> { ... }
	 */
	private generateEndpoint(group: EndpointsGroup, endpoint: Endpoint): NodeWithImportRequests<ts.MethodDeclaration>
	{
		const statements: Array<ts.Statement> = [
			this.generateUrlAssignment(group),
		];

		const comments: Array<string> = [
			`${endpoint.httpMethod} ${group.url}`,
		];

		const success = endpoint.successResponse.createTypeNode(false);
		let imports = success.imports;

		const parameters: Array<ts.ParameterDeclaration> = [];
		const body = endpoint.body;
		if (typeof body !== 'undefined') {
			const bodyType = body.createTypeNode(false);
			imports = [...imports, ...bodyType.imports];

			statements.push(this.generateBodyAssignment(endpoint, body));

			parameters.push(ts.createParameter(
				[],
				[],
				undefined,
				'resource',
				endpoint.isBodyRequired() ? undefined : ts.createToken(ts.SyntaxKind.QuestionToken),
				bodyType.node,
			));
		}

		const fetchResult = this.generateFetchResultAssignment(endpoint);

		const method = ts.createMethod(
			[],
			[
				ts.createToken(ts.SyntaxKind.PublicKeyword),
				ts.createToken(ts.SyntaxKind.AsyncKeyword),
			],
			undefined,
			endpoint.method,
			undefined,
			[],
			parameters,
			ts.createTypeReferenceNode('Promise', [success.node]),
			ts.createBlock([
				...statements,
				fetchResult.node,
				ts.createExpressionStatement(this.generateLogging(group, endpoint)),
				this.generateEndpointReturn(),
			], true),
		);

		const apiVersionKind = endpoint.apiVersionKind;
		if (typeof apiVersionKind !== 'undefined') {
			comments.push(`ApiVersion: ${apiVersionKind.apiVersion}`);
			comments.push(`Kind: ${apiVersionKind.kind}`);
		}

		if (typeof endpoint.description !== 'undefined') {
			comments.push(endpoint.description);
		}

		return createNodeWithImportRequests(addJSDoc(method, comments), [
			...imports,
			...fetchResult.imports,
		]);
	}

	/**
	 * Generates:
	 * const url = <endpointUrl>
	 */
	private generateUrlAssignment(group: EndpointsGroup): ts.VariableStatement
	{
		return ts.createVariableStatement([], ts.createVariableDeclarationList([
			ts.createVariableDeclaration(
				'url',
				undefined,
				group.createUrlFactoryNode(),
			),
		], ts.NodeFlags.Const));
	}

	/**
	 * Generates:
	 * const body = typeof resource === "undefined" ? undefined : {...<resourceToJson>, apiVersion: "<apiVersion>", kind: "<kind>"}
	 *
	 * Generates: (PATCH method)
	 * const body = <resourceToJson>
	 */
	private generateBodyAssignment(endpoint: Endpoint, body: TypedObject): ts.VariableStatement
	{
		let toJson = body.createToJsonGetterNode(ts.createIdentifier('resource'));

		if (endpoint.action !== OpenApiKubernetesAction.Patch) {
			const apiVersionKind = endpoint.apiVersionKind;
			if (typeof apiVersionKind !== 'undefined') {
				toJson = ts.createObjectLiteral([
					ts.createSpreadAssignment(toJson),
					ts.createPropertyAssignment('apiVersion', ts.createStringLiteral(apiVersionKind.apiVersion)),
					ts.createPropertyAssignment('kind', ts.createStringLiteral(apiVersionKind.kind)),
				], false);
			}
		}

		const assignment = endpoint.isBodyRequired() || endpoint.action === OpenApiKubernetesAction.Patch ?
			toJson :
			ts.createConditional(
				ts.createBinary(
					ts.createTypeOf(ts.createIdentifier('resource')),
					ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
					ts.createStringLiteral('undefined'),
				),
				ts.createToken(ts.SyntaxKind.QuestionToken),
				ts.createIdentifier('undefined'),
				ts.createToken(ts.SyntaxKind.ColonToken),
				toJson,
			);

		return ts.createVariableStatement([], ts.createVariableDeclarationList([
			ts.createVariableDeclaration(
				'body',
				undefined,
				assignment,
			),
		], ts.NodeFlags.Const));
	}

	/**
	 * Generates:
	 * const result = <ResourceClass>.createFromJson(await this.fetch.request("<httpMethod>", url, body))
	 */
	private generateFetchResultAssignment(endpoint: Endpoint): NodeWithImportRequests<ts.Statement>
	{
		let imports: Array<ImportRequest> = [];
		const args: Array<ts.Expression> = [
			ts.createStringLiteral(endpoint.httpMethod),
			ts.createIdentifier('url'),
		];

		const body = endpoint.body;
		if (typeof body !== 'undefined') {
			args.push(ts.createIdentifier('body'));
		}

		let result: ts.Expression = ts.createAwait(
			ts.createCall(
				ts.createPropertyAccess(
					ts.createPropertyAccess(
						ts.createThis(),
						'fetch',
					),
					'request',
				),
				[],
				args,
			),
		);

		const success = endpoint.successResponse;
		if (typeof success !== 'undefined') {
			const successGetter = success.createFromJsonGetterNode(result);
			imports = successGetter.imports;
			result = successGetter.node;
		}

		const node = ts.createVariableStatement([], ts.createVariableDeclarationList([
			ts.createVariableDeclaration(
				'result',
				undefined,
				result,
			),
		], ts.NodeFlags.Const));

		return createNodeWithImportRequests(node, imports);
	}

	/**
	 * Generates:
	 * await this.logger.<methodName>(url, parameters, data)
	 */
	private generateLogging(group: EndpointsGroup, endpoint: Endpoint): ts.AwaitExpression
	{
		const data: Array<ts.ObjectLiteralElementLike> = [
			ts.createShorthandPropertyAssignment('result'),
			ts.createPropertyAssignment(
				'endpoint',
				ts.createObjectLiteral([
					ts.createPropertyAssignment('method', ts.createStringLiteral(endpoint.httpMethod)),
					ts.createPropertyAssignment('urlPattern', ts.createStringLiteral(group.url)),
				], true),
			),
		];

		const body = endpoint.body;
		if (typeof body !== 'undefined') {
			data.push(ts.createShorthandPropertyAssignment('body'));

			if (body instanceof ReferenceType) {
				data.push(ts.createShorthandPropertyAssignment('resource'));
			}
		}

		const definition = group.associatedDefinition;
		if (typeof definition !== 'undefined') {
			data.push(ts.createPropertyAssignment(
				'definition',
				ts.createObjectLiteral([
					ts.createPropertyAssignment('fullName', ts.createStringLiteral(definition.name)),
					ts.createPropertyAssignment('className', ts.createStringLiteral(definition.className)),
				], true),
			));
		}

		const apiVersionKind = endpoint.apiVersionKind;
		if (typeof apiVersionKind !== 'undefined') {
			data.push(ts.createPropertyAssignment(
				'apiVersionKind',
				ts.createObjectLiteral([
					ts.createPropertyAssignment('apiVersion', ts.createStringLiteral(apiVersionKind.apiVersion)),
					ts.createPropertyAssignment('kind', ts.createStringLiteral(apiVersionKind.kind)),
				], true),
			));
		}

		return ts.createAwait(
			ts.createCall(
				ts.createPropertyAccess(
					ts.createPropertyAccess(ts.createThis(), 'logger'),
					endpoint.method,
				),
				[],
				[
					ts.createIdentifier('url'),
					ts.createObjectLiteral([
						...group.urlParameters.map((parameter) => {
							return ts.createPropertyAssignment(
								ts.createStringLiteral(parameter.name),
								ts.createElementAccess(ts.createThis(), ts.createStringLiteral(parameter.name)),
							);
						}),
					], group.urlParameters.length > 0),
					ts.createObjectLiteral(data, true),
				],
			),
		);
	}

	/**
	 * Generates:
	 * return result
	 */
	private generateEndpointReturn(): ts.ReturnStatement
	{
		return ts.createReturn(
			ts.createIdentifier('result'),
		);
	}

	/**
	 * Generates:
	 * public async patchResource(resource: <ResourceClass>): Promise<ResourceClass>
	 * {
	 * 		const previous = await this.get();
	 * 		const diff = this.jsonPatchGenerator.createForResource(previous, resource);
	 * 		return this.patch(diff);
	 * }
	 */
	private generateShortcutPatchResource(definition: Definition): NodeWithImportRequests<ts.MethodDeclaration>
	{
		const node = ts.createMethod(
			[],
			[
				ts.createToken(ts.SyntaxKind.PublicKeyword),
				ts.createToken(ts.SyntaxKind.AsyncKeyword),
			],
			undefined,
			'patchResource',
			undefined,
			[],
			[
				ts.createParameter(
					undefined,
					undefined,
					undefined,
					'resource',
					undefined,
					ts.createTypeReferenceNode(definition.className, []),
				),
			],
			ts.createTypeReferenceNode(
				ts.createIdentifier('Promise'),
				[
					ts.createTypeReferenceNode(ts.createIdentifier(definition.className), []),
				],
			),
			ts.createBlock([
				ts.createVariableStatement([], ts.createVariableDeclarationList([
					ts.createVariableDeclaration(
						'previous',
						undefined,
						ts.createAwait(
							ts.createCall(
								ts.createPropertyAccess(ts.createThis(), 'get'),
								[],
								[],
							),
						),
					),
				], ts.NodeFlags.Const)),
				ts.createVariableStatement([], ts.createVariableDeclarationList([
					ts.createVariableDeclaration(
						'diff',
						undefined,
						ts.createAwait(
							ts.createCall(
								ts.createPropertyAccess(
									ts.createPropertyAccess(ts.createThis(), 'jsonPatchGenerator'),
									'createForResource',
								),
								[],
								[
									ts.createIdentifier('previous'),
									ts.createIdentifier('resource'),
								],
							),
						),
					),
				], ts.NodeFlags.Const)),
				ts.createReturn(
					ts.createCall(
						ts.createPropertyAccess(ts.createThis(), 'patch'),
						[],
						[
							ts.createIdentifier('diff'),
						],
					),
				),
			], true),
		);

		return createNodeWithImportRequests(node, [
			{
				targetFileName: definition.fileName,
				names: [definition.className],
			},
		]);
	}

	/**
	 * Generates:
	 * public async exists(): Promise<boolean> {
	 * 		try {
	 * 			await this.read();
	 * 		} catch (e) {
	 * 			if (e instanceof ApiResponseNotFoundError) {
	 * 				return false;
	 * 			}
	 * 			throw e;
	 * 		}
	 * 		return true;
	 * }
	 */
	private generateShortcutExists(): NodeWithImportRequests<ts.MethodDeclaration>
	{
		const node = ts.createMethod(
			[],
			[
				ts.createToken(ts.SyntaxKind.PublicKeyword),
				ts.createToken(ts.SyntaxKind.AsyncKeyword),
			],
			undefined,
			'exists',
			undefined,
			[],
			[],
			ts.createTypeReferenceNode(
				'Promise',
				[
					ts.createKeywordTypeNode(ts.SyntaxKind.BooleanKeyword),
				],
			),
			ts.createBlock([
				ts.createTry(
					ts.createBlock([
						ts.createExpressionStatement(
							ts.createAwait(
								ts.createCall(
									ts.createPropertyAccess(ts.createThis(), 'get'),
									[],
									[],
								),
							),
						),
					], true),
					ts.createCatchClause(
						ts.createVariableDeclaration('e'),
						ts.createBlock([
							ts.createIf(
								ts.createBinary(
									ts.createIdentifier('e'),
									ts.createToken(ts.SyntaxKind.InstanceOfKeyword),
									ts.createIdentifier('ApiResponseNotFoundError'),
								),
								ts.createBlock([
									ts.createReturn(ts.createFalse()),
								], true),
							),
							ts.createThrow(ts.createIdentifier('e')),
						], true),
					),
					undefined,
				),
				ts.createReturn(ts.createTrue()),
			], true),
		);

		return createNodeWithImportRequests(node, [
			{
				targetFileName: 'error/index.ts',
				names: ['ApiResponseNotFoundError'],
			},
		]);
	}

	/**
	 * Generates:
	 * public async postOr<method>(resource: <ResourceClass>): Promise<<ResourceClass>> {
	 * 		if (typeof resource.metadata === "undefined" || typeof resource.metadata.name === "undefined") {
	 * 			throw new Error("PostOr<method>: resource is missing the metadata.name field");
	 * 		}
	 * 		const byName = this.byName(resource.metadata.name);
	 * 		if (await byName.exists()) {
	 * 			return byName.patch(resource);
	 * 		}
	 * 		return this.post(resource);
	 * }
	 */
	private generateShortcutPostOr(definition: Definition, orMethod: string): NodeWithImportRequests<ts.MethodDeclaration>
	{
		const node = ts.createMethod(
			[],
			[
				ts.createToken(ts.SyntaxKind.PublicKeyword),
				ts.createToken(ts.SyntaxKind.AsyncKeyword),
			],
			undefined,
			ts.createIdentifier('postOr' + firstUpper(orMethod)),
			undefined,
			[],
			[
				ts.createParameter(
					[],
					[],
					undefined,
					ts.createIdentifier('resource'),
					undefined,
					ts.createTypeReferenceNode(definition.className, []),
				),
			],
			ts.createTypeReferenceNode(ts.createIdentifier('Promise'), [
				ts.createTypeReferenceNode(definition.className, []),
			]),
			ts.createBlock([
				ts.createIf(
					ts.createBinary(
						ts.createBinary(
							ts.createTypeOf(
								ts.createPropertyAccess(ts.createIdentifier('resource'), 'metadata')
							),
							ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
							ts.createStringLiteral('undefined'),
						),
						ts.createToken(ts.SyntaxKind.BarBarToken),
						ts.createBinary(
							ts.createTypeOf(
								ts.createPropertyAccess(
									ts.createPropertyAccess(ts.createIdentifier('resource'), 'metadata'),
									'name',
								),
							),
							ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
							ts.createStringLiteral('undefined'),
						),
					),
					ts.createBlock([
						ts.createThrow(
							ts.createNew(
								ts.createIdentifier('Error'),
								[],
								[
									ts.createStringLiteral('PostOr' + firstUpper(orMethod) + ': resource is missing the metadata.name field'),
								],
							),
						),
					], true),
				),
				ts.createVariableStatement([], ts.createVariableDeclarationList([
					ts.createVariableDeclaration(
						'byName',
						undefined,
						ts.createCall(
							ts.createPropertyAccess(ts.createThis(), 'byName'),
							[],
							[
								ts.createPropertyAccess(
									ts.createPropertyAccess(ts.createIdentifier('resource'), 'metadata'),
									'name',
								),
							],
						),
					),
				], ts.NodeFlags.Const)),
				ts.createIf(
					ts.createAwait(
						ts.createCall(
							ts.createPropertyAccess(ts.createIdentifier('byName'), 'exists'),
							[],
							[],
						),
					),
					ts.createBlock([
						ts.createReturn(
							ts.createCall(
								ts.createPropertyAccess(ts.createIdentifier('byName'), orMethod === 'patch' ? 'patchResource' : orMethod),
								[],
								[
									ts.createIdentifier('resource'),
								],
							),
						),
					], true),
				),
				ts.createReturn(
					ts.createCall(
						ts.createPropertyAccess(ts.createThis(), 'post'),
						[],
						[
							ts.createIdentifier('resource'),
						],
					),
				)
			], true),
		);

		return createNodeWithImportRequests(node, [
			{
				targetFileName: definition.fileName,
				names: [definition.className],
			},
		]);
	}
}
