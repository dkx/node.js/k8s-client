import * as fs from 'fs-extra';
import * as path from 'path';


export class StaticGenerator
{
	constructor(
		private outputDir: string,
	) {}

	public async generate(): Promise<void>
	{
		await fs.copy(path.join(__dirname, '..', '..', 'static'), this.outputDir);
	}
}
