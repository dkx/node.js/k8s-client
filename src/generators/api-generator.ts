import * as fs from 'fs-extra';

import {CodeWriter} from './code-writer';
import {Document} from '../openapi';
import {DefinitionGenerator} from './definition-generator';
import {StaticGenerator} from './static-generator';
import {EndpointsGroupsGenerator} from './endpoints-groups-generator';
import {Compiler} from './compiler';


export class ApiGenerator
{
	constructor(
		private readonly writer: CodeWriter,
		private readonly doc: Document,
		private readonly outputDir: string,
	) {}

	public async generateApi(): Promise<void>
	{
		try {
			await fs.remove(this.outputDir);
		} catch (e) {}

		const definitionGenerator = new DefinitionGenerator(this.writer, this.outputDir);
		const endpointsGroupGenerator = new EndpointsGroupsGenerator(this.writer, this.outputDir);

		console.log('Generating static files...');
		await (new StaticGenerator(this.outputDir)).generate();

		process.stdout.write('Generating resources...');
		const definitions = this.doc.definitions.filter((definition) => definition.shouldBeGenerated());
		for (let definition of definitions) {
			await definitionGenerator.generateDefinition(definition);
			process.stdout.write('.');
		}
		console.log('');

		process.stdout.write('Generating endpoints...');
		const endpointGroups = this.doc.endpointsGroups;
		for (let endpointGroup of endpointGroups) {
			await endpointsGroupGenerator.generate(endpointGroup);
			process.stdout.write('.');
		}
		console.log('');

		console.log('Compiling from typescript...');
		await (new Compiler(this.outputDir)).compileAll();
	}
}
