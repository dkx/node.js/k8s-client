import * as R from 'ramda';
import * as path from 'path';

import {OpenApiDefinition, OpenApiDefinitionProperty} from './typings';
import {DefinitionProperty} from './definition-property';
import {ApiVersionKindInfo, Document} from './document';
import {camelCaseToHyphens} from '../utils';


const ROOT_DIR_NAME: string = 'resource';

export class Definition
{
	public readonly properties: Array<DefinitionProperty>;

	constructor(
		private readonly doc: Document,
		public readonly name: string,
		private readonly source: OpenApiDefinition,
	) {
		this.properties = this.mapProperties();
	}

	public get type(): string|undefined
	{
		if (typeof this.source.type === 'undefined' && this.properties.length > 0) {
			return 'object';
		}

		return this.source.type;
	}

	public get format(): string|undefined
	{
		return this.source.format;
	}

	public get description(): string|undefined
	{
		return this.source.description;
	}

	public get fileName(): string
	{
		return R.pipe(
			R.split('.'),
			R.map((part) => camelCaseToHyphens(part)),
			R.prepend(ROOT_DIR_NAME),
			R.join(path.sep),
		)(this.name) + '.ts';
	}

	public get className(): string
	{
		return R.pipe(
			R.split('.'),
			R.last,
		)(this.name);
	}

	public get optionsClassName(): string
	{
		return R.pipe(
			R.split('.'),
			R.last,
		)(this.name) + 'Options';
	}

	public get apiVersionKindList(): Array<ApiVersionKindInfo>
	{
		if (!Array.isArray(this.source['x-kubernetes-group-version-kind'])) {
			return [];
		}

		return this.source['x-kubernetes-group-version-kind'].map((data) => {
			return {
				apiVersion: data.group === '' ? data.version : data.group + '/' + data.version,
				kind: data.kind,
			};
		});
	}

	public hasRequiredProperty(): boolean
	{
		const required = this.properties.find((property) => property.required);
		return typeof required !== 'undefined';
	}

	public isSimple(): boolean
	{
		return this.type === 'object' && this.properties.length === 0;
	}

	public shouldBeGenerated(): boolean
	{
		// todo: remove?
		if (Array.isArray(this.source['x-kubernetes-group-version-kind']) && this.source['x-kubernetes-group-version-kind'].length > 0) {
			return true;
		}

		return this.type === 'object' && this.properties.length > 0;
	}

	public hasApiVersionAndKind(apiVersion: string, kind: string): boolean
	{
		const info = this.apiVersionKindList.find((info) => info.apiVersion === apiVersion && info.kind === kind);
		return typeof info !== 'undefined';
	}

	private mapProperties(): Array<DefinitionProperty>
	{
		return R.pipe(
			R.mapObjIndexed((property: OpenApiDefinitionProperty, name: string): DefinitionProperty => {
				const isRequired = (this.source.required || []).indexOf(name) >= 0;
				return new DefinitionProperty(this.doc, name, isRequired, property);
			}),
			R.values,
			R.sort((a: DefinitionProperty, b: DefinitionProperty) => {
				if (a.required && !b.required) {
					return -1;
				}

				if (!a.required && b.required) {
					return 1;
				}

				const aName = a.name.toLowerCase();
				const bName = b.name.toLowerCase();

				if (aName < bName) {
					return -1;
				}

				if (aName > bName) {
					return 1;
				}

				return 0;
			}),
			R.filter((property) => property.name !== 'apiVersion' && property.name !== 'kind'),
		)(this.source.properties || {});
	}
}
