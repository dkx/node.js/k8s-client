import * as ts from 'typescript';

import {OpenApiDefinitionProperty} from './typings';
import {Document} from './document';
import {createTypedObject, TypedObject} from './types';
import {NodeWithImportRequests} from '../utils';


export class DefinitionProperty
{
	constructor(
		private readonly doc: Document,
		public readonly name: string,
		public readonly required: boolean,
		protected readonly source: OpenApiDefinitionProperty,
	) {}

	public get description(): string|undefined
	{
		return this.source.description;
	}

	public get type(): TypedObject
	{
		return createTypedObject(this.doc, this.source);
	}

	public createTypeNode(withOptions: boolean): NodeWithImportRequests<ts.TypeNode>
	{
		return this.type.createTypeNode(withOptions);
	}

	public createOptionsGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		return this.type.createOptionsGetterNode(accessor);
	}

	public createFromJsonGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		return this.type.createFromJsonGetterNode(accessor);
	}

	public createToJsonGetterNode(accessor: ts.Expression): ts.Expression
	{
		return this.type.createToJsonGetterNode(accessor);
	}
}
