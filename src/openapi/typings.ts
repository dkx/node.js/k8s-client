export enum OpenApiKubernetesAction
{
	List = 'list',
	Get = 'get',
	Post = 'post',
	Patch = 'patch',
	Put = 'put',
	Delete = 'delete',
	DeleteCollection = 'deletecollection',
	Connect = 'connect',
}

export declare interface OpenApiKubernetesApiVersionKind
{
	group: string,
	version: string,
	kind: string,
}

export declare interface OpenApiTypedObject
{
	$ref?: string,
	type?: string,
	format?: string,
	items?: {
		$ref?: string,
		type?: string,
	},
	additionalProperties?: {
		$ref?: string,
		type?: string,
	},
}

export declare interface OpenApiDefinitionProperty extends OpenApiTypedObject
{
	description?: string,
}

export declare interface OpenApiDefinition
{
	type?: string,
	format?: string,
	description?: string,
	properties?: {[name: string]: OpenApiDefinitionProperty},
	required?: Array<string>,
	'x-kubernetes-group-version-kind'?: Array<OpenApiKubernetesApiVersionKind>,
}

export declare interface OpenApiParameter
{
	name: string,
	in: string,
	description?: string,
	type?: string,
	required?: boolean,
	schema?: OpenApiTypedObject,
}

export declare interface OpenApiEndpointResponse
{
	description?: string,
	schema?: OpenApiTypedObject,
}

export declare interface OpenApiEndpoint
{
	description?: string,
	parameters?: Array<OpenApiParameter>,
	responses?: {[code: string]: OpenApiEndpointResponse},
	'x-kubernetes-action'?: OpenApiKubernetesAction,
	'x-kubernetes-group-version-kind'?: OpenApiKubernetesApiVersionKind,
}

export declare interface OpenApiPath
{
	get?: OpenApiEndpoint,
	post?: OpenApiEndpoint,
	put?: OpenApiEndpoint,
	delete?: OpenApiEndpoint,
	patch?: OpenApiEndpoint,
	parameters?: Array<OpenApiParameter>,
}

export declare interface OpenApiDocument
{
	definitions?: {[name: string]: OpenApiDefinition},
	paths?: {[path: string]: OpenApiPath},
}
