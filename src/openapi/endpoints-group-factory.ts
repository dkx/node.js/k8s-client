import * as path from 'path';

import {EndpointsGroup} from './endpoints-group';
import {hyphensToCamelCase} from '../utils';


export class EndpointsGroupFactory
{
	constructor(
		public readonly targetGroup: EndpointsGroup,
	) {}

	public get methodName(): string
	{
		return hyphensToCamelCase(path.basename(this.targetGroup.fileName, '.ts')).slice(0, -3);
	}
}
