import {OpenApiParameter, OpenApiTypedObject} from './typings';


export class EndpointParameter
{
	constructor(
		private readonly source: OpenApiParameter,
	) {}

	public get name(): string
	{
		return this.source.name;
	}

	public get in(): string
	{
		return this.source.in;
	}

	public get required(): boolean
	{
		if (typeof this.source.required === 'undefined') {
			return false;
		}

		return this.source.required;
	}

	public get schema(): OpenApiTypedObject|undefined
	{
		return this.source.schema;
	}
}
