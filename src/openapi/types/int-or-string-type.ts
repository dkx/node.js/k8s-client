import * as ts from 'typescript';

import {TypedObject} from './typed-object';
import {createNodeWithImportRequests, NodeWithImportRequests} from '../../utils';


export class IntOrStringType implements TypedObject
{
	public createTypeNode(withOptions: boolean): NodeWithImportRequests<ts.TypeNode>
	{
		return createNodeWithImportRequests(ts.createUnionTypeNode([
			ts.createKeywordTypeNode(ts.SyntaxKind.NumberKeyword),
			ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
		]));
	}

	public createOptionsGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		return createNodeWithImportRequests(accessor);
	}

	public createFromJsonGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		return createNodeWithImportRequests(accessor);
	}

	public createToJsonGetterNode(accessor: ts.Expression): ts.Expression
	{
		return accessor;
	}
}
