import * as ts from 'typescript';

import {TypedObject} from './typed-object';
import {OpenApiTypedObject} from '../typings';
import {Document} from '../document';
import {createTypedObject} from './factories';
import {createNodeWithImportRequests, NodeWithImportRequests} from '../../utils';


export class ArrayType implements TypedObject
{
	constructor(
		private readonly doc: Document,
		private readonly innerSource: OpenApiTypedObject,
	) {}

	public get innerType(): TypedObject
	{
		return createTypedObject(this.doc, this.innerSource);
	}

	/**
	 * Generates:
	 * Array<<innerType>>
	 */
	public createTypeNode(withOptions: boolean): NodeWithImportRequests<ts.TypeNode>
	{
		const inner = this.innerType.createTypeNode(withOptions);
		return createNodeWithImportRequests(
			ts.createArrayTypeNode(inner.node),
			inner.imports,
		);
	}

	public createOptionsGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		const inner = this.innerType.createOptionsGetterNode(ts.createIdentifier('item'));
		const node = this.createIterator(accessor, inner.node);

		return createNodeWithImportRequests(node, inner.imports);
	}

	public createFromJsonGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		const inner = this.innerType.createFromJsonGetterNode(ts.createIdentifier('item'));
		const node = this.createIterator(accessor, inner.node);

		return createNodeWithImportRequests(node, inner.imports);
	}

	public createToJsonGetterNode(accessor: ts.Expression): ts.Expression
	{
		const inner = this.innerType.createToJsonGetterNode(ts.createIdentifier('item'));
		return this.createIterator(accessor, inner);
	}

	/**
	 * Generates:
	 * <propertyAccessor>.map((item) => {
	 * 		return <innerPropertyAccessor>;
	 * })
	 */
	private createIterator(accessor: ts.Expression, assignment: ts.Expression): ts.CallExpression
	{
		return ts.createCall(
			ts.createPropertyAccess(accessor, 'map'),
			[],
			[
				ts.createArrowFunction(
					[],
					[],
					[
						ts.createParameter(
							[],
							[],
							undefined,
							'item',
						),
					],
					undefined,
					ts.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
					ts.createBlock([
						ts.createReturn(assignment),
					], true),
				),
			],
		);
	}
}
