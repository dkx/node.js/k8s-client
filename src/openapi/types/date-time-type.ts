import * as ts from 'typescript';

import {TypedObject} from './typed-object';
import {createNodeWithImportRequests, NodeWithImportRequests} from '../../utils';


export class DateTimeType implements TypedObject
{
	/**
	 * Generates:
	 * Date
	 */
	public createTypeNode(withOptions: boolean): NodeWithImportRequests<ts.TypeNode>
	{
		return createNodeWithImportRequests(ts.createTypeReferenceNode('Date', []));
	}

	/**
	 * Generates:
	 * <propertyAccessor>
	 */
	public createOptionsGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		return createNodeWithImportRequests(accessor);
	}

	/**
	 * Generates:
	 * new Date(<propertyAccessor>)
	 */
	public createFromJsonGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		const node = ts.createNew(
			ts.createIdentifier('Date'),
			[],
			[accessor],
		);

		return createNodeWithImportRequests(node);
	}

	/**
	 * Generates:
	 * <propertyAccessor>.toISOString()
	 */
	public createToJsonGetterNode(accessor: ts.Expression): ts.Expression
	{
		return ts.createCall(
			ts.createPropertyAccess(
				accessor,
				ts.createIdentifier('toISOString'),
			),
			[],
			[],
		);
	}
}
