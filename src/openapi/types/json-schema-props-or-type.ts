import {Document} from '../document';
import {Definition} from '../definition';


export abstract class JsonSchemaPropsOrType
{
	constructor(
		private doc: Document,
		private version: string,
	) {}

	protected getRootReference(): Definition
	{
		return this.doc.getDefinitionByName(`io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.${this.version}.JSONSchemaProps`);
	}
}
