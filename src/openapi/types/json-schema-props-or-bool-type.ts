import * as ts from 'typescript';

import {TypedObject} from './typed-object';
import {createNodeWithImportRequests, NodeWithImportRequests} from '../../utils';
import {JsonSchemaPropsOrType} from './json-schema-props-or-type';


export class JsonSchemaPropsOrBoolType extends JsonSchemaPropsOrType implements TypedObject
{
	/**
	 * Generates:
	 * boolean|<ResourceClass>
	 */
	public createTypeNode(withOptions: boolean): NodeWithImportRequests<ts.TypeNode>
	{
		const ref = this.getRootReference();
		return createNodeWithImportRequests(
			ts.createUnionTypeNode([
				ts.createKeywordTypeNode(ts.SyntaxKind.BooleanKeyword),
				ts.createTypeReferenceNode(ref.className, []),
			]),
			[
				{
					targetFileName: ref.fileName,
					names: [ref.className],
				},
			],
		);
	}

	/**
	 * Generates:
	 * <accessor>
	 */
	public createOptionsGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		return createNodeWithImportRequests(accessor);
	}

	/**
	 * Generates:
	 * typeof <propertyAccessor> === "boolean" ? <propertyAccessor> : <ResourceName>.createFromJson(<propertyAccessor>)
	 */
	public createFromJsonGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		const ref = this.getRootReference();
		const node = ts.createConditional(
			ts.createBinary(
				ts.createTypeOf(accessor),
				ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
				ts.createStringLiteral('boolean'),
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			accessor,
			ts.createToken(ts.SyntaxKind.ColonToken),
			ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier(ref.className), 'createFromJson'),
				[],
				[accessor],
			),
		);

		return createNodeWithImportRequests(node, [
			{
				targetFileName: ref.fileName,
				names: [ref.className],
			},
		]);
	}

	/**
	 * Generates:
	 * typeof <propertyAccessor> === "boolean" ? <propertyAccessor> : <propertyAccessor>.toJson()
	 */
	public createToJsonGetterNode(accessor: ts.Expression): ts.Expression
	{
		return ts.createConditional(
			ts.createBinary(
				ts.createTypeOf(accessor),
				ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
				ts.createStringLiteral('boolean'),
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			accessor,
			ts.createToken(ts.SyntaxKind.ColonToken),
			ts.createCall(
				ts.createPropertyAccess(accessor, 'toJson'),
				[],
				[],
			),
		);
	}
}
