import * as ts from 'typescript';

import {TypedObject} from './typed-object';
import {createNodeWithImportRequests, NodeWithImportRequests} from '../../utils';


const ALLOWED_STRING_FORMATS: Array<string> = ['byte'];
const ALLOWED_NUMBER_FORMATS: Array<string> = ['double'];
const ALLOWED_INTEGER_FORMATS: Array<string> = ['int32', 'int64'];

export class PrimitiveType implements TypedObject
{
	constructor(
		private readonly type: string,
		private readonly format?: string,
	) {}

	/**
	 * Generates:
	 * <primitiveType>
	 */
	public createTypeNode(withOptions: boolean): NodeWithImportRequests<ts.TypeNode>
	{
		if (this.type === 'boolean') {
			if (typeof this.format !== 'undefined') {
				throw createUnknownFormatError(this.type, this.format, [], true);
			}

			return createNodeWithImportRequests(ts.createKeywordTypeNode(ts.SyntaxKind.BooleanKeyword));
		}

		if (this.type === 'string') {
			if (typeof this.format !== 'undefined' && ALLOWED_STRING_FORMATS.indexOf(this.format) < 0) {
				throw createUnknownFormatError(this.type, this.format, ALLOWED_STRING_FORMATS, true);
			}

			return createNodeWithImportRequests(ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword));
		}

		if (this.type === 'number') {
			if (typeof this.format === 'undefined') {
				throw createMissingFormatError(this.type, ALLOWED_NUMBER_FORMATS);
			}

			if (ALLOWED_NUMBER_FORMATS.indexOf(this.format) < 0) {
				throw createUnknownFormatError(this.type, this.format, ALLOWED_NUMBER_FORMATS, false);
			}

			return createNodeWithImportRequests(ts.createKeywordTypeNode(ts.SyntaxKind.NumberKeyword));
		}

		if (this.type === 'integer') {
			if (typeof this.format === 'undefined') {
				throw createMissingFormatError(this.type, ALLOWED_INTEGER_FORMATS);
			}

			if (ALLOWED_INTEGER_FORMATS.indexOf(this.format) < 0) {
				throw createUnknownFormatError(this.type, this.format, ALLOWED_INTEGER_FORMATS, false);
			}

			return createNodeWithImportRequests(ts.createKeywordTypeNode(ts.SyntaxKind.NumberKeyword));
		}

		throw new Error(`Definition property type "${this.type}" is not supported`);
	}

	/**
	 * Generates:
	 * <propertyAccessor>
	 */
	public createOptionsGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		return createNodeWithImportRequests(accessor);
	}

	/**
	 * Generates:
	 * <propertyAccessor>
	 */
	public createFromJsonGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		return createNodeWithImportRequests(accessor);
	}

	/**
	 * Generates:
	 * <propertyAccessor>
	 */
	public createToJsonGetterNode(accessor: ts.Expression): ts.Expression
	{
		return accessor;
	}
}

function createUnknownFormatError(type: string, format: string, allowedFormats: Array<string>, allowNone: boolean): Error
{
	let formats = allowedFormats.map((format) => `"${format}"`).join(', ');
	if (allowNone) {
		formats += ' or none';
	}

	const suffix = allowedFormats.length === 0 ?
		'but it should not have any' :
		`it should be ${formats}`;

	throw new Error(`Definition property type "${type}" contains unexpected format "${format}", ${suffix}`);
}

function createMissingFormatError(type: string, allowedFormats: Array<string>): Error
{
	const formats = allowedFormats.map((format) => `"${format}"`).join(', ');
	return new Error(`Definition property type "${type}" does not contain a required format, it should be ${formats}`);
}
