import * as ts from 'typescript';

import {NodeWithImportRequests} from '../../utils';


export interface TypedObject
{
	createTypeNode(withOptions: boolean): NodeWithImportRequests<ts.TypeNode>;

	createOptionsGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>;

	createFromJsonGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>;

	createToJsonGetterNode(accessor: ts.Expression): ts.Expression;
}
