import * as ts from 'typescript';

import {TypedObject} from './typed-object';
import {Document} from '../document';
import {createNodeWithImportRequests, ImportRequest, NodeWithImportRequests} from '../../utils';
import {Definition} from '../definition';


export class ReferenceType implements TypedObject
{
	constructor(
		private doc: Document,
		private referenceName: string,
	) {}

	public getDefinition(): Definition
	{
		return this.doc.getDefinitionByReference(this.referenceName);
	}

	/**
	 * Generates:
	 * <ResourceClass>|<ResourceOptionsClass>
	 */
	public createTypeNode(withOptions: boolean): NodeWithImportRequests<ts.TypeNode>
	{
		const ref = this.getDefinition();
		if (ref.isSimple()) {
			withOptions = false;
		}

		const type = ts.createTypeReferenceNode(ref.className, []);

		const importRequest: ImportRequest = {
			targetFileName: ref.fileName,
			names: [ref.className],
		};

		if (!withOptions) {
			return createNodeWithImportRequests(type, [importRequest]);
		}

		importRequest.names.push(ref.optionsClassName);

		const node = ts.createUnionTypeNode([
			type,
			ts.createTypeReferenceNode(ref.optionsClassName, []),
		]);

		return createNodeWithImportRequests(node, [importRequest]);
	}

	/**
	 * Generates:
	 * <accessor> instanceof <ResourceName> ? <accessor> : new <ResourceName>(<accessor>)
	 */
	public createOptionsGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		const ref = this.getDefinition();
		const importRequest: ImportRequest = {
			targetFileName: ref.fileName,
			names: [ref.className],
		};

		if (ref.isSimple()) {
			return createNodeWithImportRequests(accessor, [importRequest]);
		}

		const node = ts.createConditional(
			ts.createBinary(
				accessor,
				ts.createToken(ts.SyntaxKind.InstanceOfKeyword),
				ts.createIdentifier(ref.className),
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			accessor,
			ts.createToken(ts.SyntaxKind.ColonToken),
			ts.createNew(
				ts.createIdentifier(ref.className),
				[],
				[
					accessor,
				],
			),
		);

		return createNodeWithImportRequests(node, [importRequest]);
	}

	/**
	 * Generates:
	 * <ResourceName>.createFromJson(<propertyAccessor>)
	 */
	public createFromJsonGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		const ref = this.getDefinition();
		const node = ts.createCall(
			ts.createPropertyAccess(ts.createIdentifier(ref.className), 'createFromJson'),
			[],
			[accessor],
		);

		return createNodeWithImportRequests(node, [
			{
				targetFileName: ref.fileName,
				names: [ref.className],
			},
		]);
	}

	/**
	 * Generates:
	 * <propertyAccessor>.toJson()
	 */
	public createToJsonGetterNode(accessor: ts.Expression): ts.Expression
	{
		return ts.createCall(
			ts.createPropertyAccess(accessor, 'toJson'),
			[],
			[],
		);
	}
}
