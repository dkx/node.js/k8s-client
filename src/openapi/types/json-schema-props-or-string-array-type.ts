import * as ts from 'typescript';

import {TypedObject} from './typed-object';
import {createNodeWithImportRequests, NodeWithImportRequests} from '../../utils';
import {JsonSchemaPropsOrType} from './json-schema-props-or-type';


export class JsonSchemaPropsOrStringArrayType extends JsonSchemaPropsOrType implements TypedObject
{
	/**
	 * Generates:
	 * <ResourceClass>|Array<string>
	 */
	public createTypeNode(withOptions: boolean): NodeWithImportRequests<ts.TypeNode>
	{
		const ref = this.getRootReference();
		return createNodeWithImportRequests(
			ts.createUnionTypeNode([
				ts.createArrayTypeNode(ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword)),
				ts.createTypeReferenceNode(ref.className, []),
			]),
			[
				{
					targetFileName: ref.fileName,
					names: [ref.className],
				},
			],
		);
	}

	/**
	 * Generates:
	 * <accessor>
	 */
	public createOptionsGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		return createNodeWithImportRequests(accessor);
	}

	/**
	 * Generates:
	 * Array.isArray(<accessor>) ? <propertyAccessor> : <ResourceName>.createFromJson(<propertyAccessor>)
	 */
	public createFromJsonGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		const ref = this.getRootReference();
		return createNodeWithImportRequests(
			ts.createConditional(
				ts.createCall(
					ts.createPropertyAccess(ts.createIdentifier('Array'), 'isArray'),
					[],
					[accessor],
				),
				ts.createToken(ts.SyntaxKind.QuestionToken),
				accessor,
				ts.createToken(ts.SyntaxKind.ColonToken),
				ts.createCall(
					ts.createPropertyAccess(ts.createIdentifier(ref.className), 'createFromJson'),
					[],
					[accessor],
				),
			),
			[
				{
					targetFileName: ref.fileName,
					names: [ref.className],
				},
			],
		);
	}

	/**
	 * Generates:
	 * Array.isArray(<accessor>) ? <propertyAccessor> : <propertyAccessor>.toJson()
	 */
	public createToJsonGetterNode(accessor: ts.Expression): ts.Expression
	{
		return ts.createConditional(
			ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier('Array'), 'isArray'),
				[],
				[accessor],
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			accessor,
			ts.createToken(ts.SyntaxKind.ColonToken),
			ts.createCall(
				ts.createPropertyAccess(accessor, 'toJson'),
				[],
				[],
			),
		);
	}
}
