import * as ts from 'typescript';

import {TypedObject} from './typed-object';
import {createNodeWithImportRequests, NodeWithImportRequests} from '../../utils';
import {JsonSchemaPropsOrType} from './json-schema-props-or-type';


export class JsonSchemaPropsOrArrayType extends JsonSchemaPropsOrType implements TypedObject
{
	/**
	 * Generates:
	 * <ResourceClass>|Array<ResourceClass>
	 */
	public createTypeNode(withOptions: boolean): NodeWithImportRequests<ts.TypeNode>
	{
		const ref = this.getRootReference();
		const type = ts.createTypeReferenceNode(ref.className, []);

		return createNodeWithImportRequests(
			ts.createUnionTypeNode([
				type,
				ts.createArrayTypeNode(type),
			]),
			[
				{
					targetFileName: ref.fileName,
					names: [ref.className],
				},
			],
		);
	}

	/**
	 * Generates:
	 * <accessor>
	 */
	public createOptionsGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		return createNodeWithImportRequests(accessor);
	}

	/**
	 * Generates:
	 * Array.isArray(<accessor>) ? <propertyAccessor>.map((item) => <ResourceName>.createFromJson(item)) : <ResourceName>.createFromJson(<propertyAccessor>)
	 */
	public createFromJsonGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		const ref = this.getRootReference();
		const createFactory = (accessor: ts.Expression): ts.CallExpression => {
			return ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier(ref.className), 'createFromJson'),
				[],
				[accessor],
			);
		};

		const node = ts.createConditional(
			ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier('Array'), 'isArray'),
				[],
				[accessor],
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			ts.createCall(
				ts.createPropertyAccess(accessor, 'map'),
				[],
				[
					ts.createArrowFunction(
						[],
						[],
						[
							ts.createParameter(
								[],
								[],
								undefined,
								'item',
							),
						],
						undefined,
						ts.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
						createFactory(ts.createIdentifier('item')),
					),
				],
			),
			ts.createToken(ts.SyntaxKind.ColonToken),
			createFactory(accessor),
		);

		return createNodeWithImportRequests(node, [
			{
				targetFileName: ref.fileName,
				names: [ref.className],
			},
		]);
	}

	/**
	 * Generates:
	 * Array.isArray(<accessor>) ? <propertyAccessor>.map((item) => item.toJson()) : <propertyAccessor>.toJson()
	 */
	public createToJsonGetterNode(accessor: ts.Expression): ts.Expression
	{
		const createTransformer = (accessor: ts.Expression): ts.CallExpression => {
			return ts.createCall(
				ts.createPropertyAccess(accessor, 'toJson'),
				[],
				[],
			);
		};

		return ts.createConditional(
			ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier('Array'), 'isArray'),
				[],
				[accessor],
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			ts.createCall(
				ts.createPropertyAccess(accessor, 'map'),
				[],
				[
					ts.createArrowFunction(
						[],
						[],
						[
							ts.createParameter(
								[],
								[],
								undefined,
								'item',
							),
						],
						undefined,
						ts.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
						createTransformer(ts.createIdentifier('item')),
					),
				],
			),
			ts.createToken(ts.SyntaxKind.ColonToken),
			createTransformer(accessor),
		);
	}
}
