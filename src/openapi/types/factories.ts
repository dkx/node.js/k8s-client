import {Document} from '../document';
import {OpenApiTypedObject} from '../typings';
import {TypedObject} from './typed-object';
import {IntOrStringType} from './int-or-string-type';
import {AnyType} from './any-type';
import {ArrayType} from './array-type';
import {DateTimeType} from './date-time-type';
import {JsonSchemaPropsOrArrayType} from './json-schema-props-or-array-type';
import {JsonSchemaPropsOrBoolType} from './json-schema-props-or-bool-type';
import {JsonSchemaPropsOrStringArrayType} from './json-schema-props-or-string-array-type';
import {ObjectType} from './object-type';
import {PrimitiveType} from './primitive-type';
import {ReferenceType} from './reference-type';


export function createTypedObject(doc: Document, source: OpenApiTypedObject): TypedObject
{
	if (source.$ref === '#/definitions/io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrBool') {
		return new JsonSchemaPropsOrBoolType(doc, 'v1beta1');
	}

	if (source.$ref === '#/definitions/io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrStringArray') {
		return new JsonSchemaPropsOrStringArrayType(doc, 'v1beta1');
	}

	if (source.$ref === '#/definitions/io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrArray') {
		return new JsonSchemaPropsOrArrayType(doc, 'v1beta1');
	}

	if (source.$ref === '#/definitions/io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrBool') {
		return new JsonSchemaPropsOrBoolType(doc, 'v1');
	}

	if (source.$ref === '#/definitions/io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrStringArray') {
		return new JsonSchemaPropsOrStringArrayType(doc, 'v1');
	}

	if (source.$ref === '#/definitions/io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrArray') {
		return new JsonSchemaPropsOrArrayType(doc, 'v1');
	}

	if (typeof source.$ref !== 'undefined') {
		const reference = doc.getDefinitionByReference(source.$ref);

		if (reference.name === 'io.k8s.apimachinery.pkg.apis.meta.v1.Patch') {
			return new ArrayType(doc, {
				type: 'object',
			});
		}

		if (reference.type === 'string' && reference.format === 'date-time') {
			return new DateTimeType();
		}

		if (reference.type === 'string' && reference.format === 'int-or-string') {
			return new IntOrStringType();
		}

		if (reference.type === 'string') {
			return new PrimitiveType('string', reference.format);
		}

		if (!reference.shouldBeGenerated()) {
			return new AnyType();
		}

		return new ReferenceType(doc, source.$ref);
	}

	if (source.type === 'array') {
		if (typeof source.items === 'undefined') {
			throw new Error(`Definition property of type "array" is missing the "items"`);
		}

		return new ArrayType(doc, source.items);
	}

	if (source.type === 'object') {
		if (typeof source.additionalProperties === 'undefined') {
			return new AnyType();
		}

		return new ObjectType(doc, source.additionalProperties);
	}

	if (typeof source.type !== 'undefined') {
		return new PrimitiveType(source.type, source.format);
	}

	throw new Error(`Definition property is not supported`);
}
