import * as ts from 'typescript';

import {Document} from '../document';
import {OpenApiTypedObject} from '../typings';
import {TypedObject} from './typed-object';
import {createTypedObject} from './factories';
import {createNodeWithImportRequests, NodeWithImportRequests} from '../../utils';


export class ObjectType implements TypedObject
{
	constructor(
		private readonly doc: Document,
		private readonly innerSource: OpenApiTypedObject,
	) {}

	public get innerType(): TypedObject
	{
		return createTypedObject(this.doc, this.innerSource);
	}

	/**
	 * Generates:
	 * {[key: string]: <innerType>}
	 */
	public createTypeNode(withOptions: boolean): NodeWithImportRequests<ts.TypeNode>
	{
		const inner = this.innerType.createTypeNode(withOptions);
		const node = ts.createTypeLiteralNode([
			ts.createIndexSignature(
				[],
				[],
				[
					ts.createParameter(
						[],
						[],
						undefined,
						'key',
						undefined,
						ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
					),
				],
				inner.node,
			),
		]);

		return createNodeWithImportRequests(node, inner.imports);
	}

	public createOptionsGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		const inner = this.innerType.createOptionsGetterNode(ts.createIdentifier('item'));
		const node = this.createIterator(accessor, inner.node);

		return createNodeWithImportRequests(node, inner.imports);
	}

	public createFromJsonGetterNode(accessor: ts.Expression): NodeWithImportRequests<ts.Expression>
	{
		const inner = this.innerType.createFromJsonGetterNode(ts.createIdentifier('item'));
		const node = this.createIterator(accessor, inner.node);

		return createNodeWithImportRequests(node, inner.imports);
	}

	public createToJsonGetterNode(accessor: ts.Expression): ts.Expression
	{
		const inner = this.innerType.createToJsonGetterNode(ts.createIdentifier('item'));
		return this.createIterator(accessor, inner);
	}

	/**
	 * Generates:
	 * (() => {
	 *     const data = {};
	 *     for (let key in items) {
	 *         if (items.hasOwnProperty(key)) {
	 *             let item = items[key];
	 *             data[key] = <innerPropertyAccessor>;
	 *         }
	 *     }
	 *     return data;
	 * })(<propertyAccessor>)
	 */
	private createIterator(accessor: ts.Expression, assignment: ts.Expression): ts.CallExpression
	{
		return ts.createCall(
			ts.createParen(
				ts.createArrowFunction(
					[],
					[],
					[
						ts.createParameter(
							[],
							[],
							undefined,
							'items',
						),
					],
					undefined,
					ts.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
					ts.createBlock([
						ts.createVariableStatement([], ts.createVariableDeclarationList([
							ts.createVariableDeclaration(
								'data',
								undefined,
								ts.createObjectLiteral([], false),
							),
						], ts.NodeFlags.Const)),
						ts.createForIn(
							ts.createVariableDeclarationList([
								ts.createVariableDeclaration('key'),
							], ts.NodeFlags.Let),
							ts.createIdentifier('items'),
							ts.createBlock([
								ts.createIf(
									ts.createCall(
										ts.createPropertyAccess(ts.createIdentifier('items'), 'hasOwnProperty'),
										[],
										[
											ts.createIdentifier('key'),
										],
									),
									ts.createBlock([
										ts.createVariableStatement([], ts.createVariableDeclarationList([
											ts.createVariableDeclaration(
												'item',
												undefined,
												ts.createElementAccess(ts.createIdentifier('items'), ts.createIdentifier('key')),
											),
										], ts.NodeFlags.Let)),
										ts.createExpressionStatement(
											ts.createBinary(
												ts.createElementAccess(ts.createIdentifier('data'), ts.createIdentifier('key')),
												ts.createToken(ts.SyntaxKind.EqualsToken),
												assignment,
											),
										),
									], true),
								),
							], true),
						),
						ts.createReturn(ts.createIdentifier('data')),
					], true),
				),
			),
			[],
			[accessor],
		);
	}
}
