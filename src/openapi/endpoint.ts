import {OpenApiEndpoint, OpenApiEndpointResponse, OpenApiKubernetesAction} from './typings';
import {EndpointParameter} from './endpoint-parameter';
import {createTypedObject, TypedObject} from './types';
import {ApiVersionKindInfo, Document} from './document';
import {AnyType} from './types/any-type';
import {Definition} from './definition';


export class Endpoint
{
	constructor(
		private readonly doc: Document,
		public readonly method: string,
		public readonly parameters: Array<EndpointParameter>,
		private readonly source: OpenApiEndpoint,
	) {}

	public get description(): string|undefined
	{
		return this.source.description;
	}

	public get httpMethod(): string
	{
		return this.method.toUpperCase();
	}

	public get body(): TypedObject|undefined
	{
		const body = this.parameters.find((parameter) => parameter.name === 'body' && parameter.in === 'body');
		if (typeof body === 'undefined') {
			return;
		}

		if (typeof body.schema === 'undefined') {
			return new AnyType();
		}

		return createTypedObject(this.doc, body.schema);
	}

	public get successResponse(): TypedObject
	{
		const response = this.getResponse('200');
		if (typeof response === 'undefined' || typeof response.schema === 'undefined') {
			return new AnyType();
		}

		return createTypedObject(this.doc, response.schema);
	}

	public get action(): OpenApiKubernetesAction|undefined
	{
		return this.source['x-kubernetes-action'];
	}

	public get associatedDefinition(): Definition|undefined
	{
		const info = this.apiVersionKind;
		if (typeof info === 'undefined') {
			return;
		}

		return this.doc.findDefinitionByApiVersionAndKind(info.apiVersion, info.kind);
	}

	public get apiVersionKind(): ApiVersionKindInfo|undefined
	{
		const info = this.source['x-kubernetes-group-version-kind'];
		if (typeof info === 'undefined') {
			return;
		}

		return {
			apiVersion: info.group === '' ? info.version : info.group + '/' + info.version,
			kind: info.kind,
		};
	}

	public isBodyRequired(): boolean
	{
		const body = this.parameters.find((parameter) => parameter.name === 'body' && parameter.in === 'body');
		if (typeof body === 'undefined') {
			return false;
		}

		return body.required;
	}

	private getResponse(code: string): OpenApiEndpointResponse|undefined
	{
		const responses = this.source.responses || {};
		for (let key in responses) {
			if (responses.hasOwnProperty(key) && key === code) {
				return responses[key];
			}
		}
	}
}
