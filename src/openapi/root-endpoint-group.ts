import {EndpointsGroup} from './endpoints-group';


export class RootEndpointGroup extends EndpointsGroup
{
	constructor()
	{
		super('client.ts');
	}

	public get className(): string
	{
		return 'Client';
	}
}
