import * as ts from 'typescript';
import * as R from 'ramda';
import * as path from 'path';

import {Endpoint} from './endpoint';
import {EndpointParameter} from './endpoint-parameter';
import {createStringConcatenation, firstUpper, hyphensToCamelCase} from '../utils';
import {Definition} from './definition';
import {EndpointsGroupFactory} from './endpoints-group-factory';
import {OpenApiKubernetesAction} from './typings';


export class EndpointsGroup
{
	public url: string;

	public parameters: Array<EndpointParameter> = [];

	public urlParameters: Array<EndpointParameter> = [];

	public endpoints: Array<Endpoint> = [];

	public factories: Array<EndpointsGroupFactory> = [];

	public factoryParameter: EndpointParameter|undefined;

	constructor(
		public readonly fileName: string,
	) {}

	public get className(): string
	{
		const file = path.basename(this.fileName, '.ts');
		return firstUpper(hyphensToCamelCase(file));
	}

	public get associatedDefinition(): Definition|undefined
	{
		const dependencies = R.pipe(
			R.map((endpoint: Endpoint): Definition|undefined => endpoint.associatedDefinition),
			R.filter((definition: Definition|undefined): boolean => typeof definition !== 'undefined'),
			R.uniq,
		)(this.endpoints);

		if (dependencies.length === 1) {
			return dependencies[0];
		}
	}

	public createUrlFactoryNode(): ts.Expression
	{
		if (typeof this.url === 'undefined') {
			throw new Error(`Endpoint group "${this.fileName}:${this.className}": can not create url factory node`);
		}

		const parts = this.url
			.replace(/^\//, '')
			.split('/')
			.reduce((res, part) => [...res, '/', part], []);

		const nodes = parts.map((part) => {
			part = part.replace(/@/g, '/');

			const match = /^{([a-z]+)}$/.exec(part);
			if (match === null) {
				return ts.createStringLiteral(part)
			}

			return ts.createPropertyAccess(ts.createThis(), ts.createIdentifier(match[1]));
		});

		return createStringConcatenation(nodes);
	}

	public getResourceForPatchResourceShortcut(): Definition|undefined
	{
		const get = this.endpoints.find((endpoint) => endpoint.action === OpenApiKubernetesAction.Get);
		const patch = this.endpoints.find((endpoint) => endpoint.action === OpenApiKubernetesAction.Patch);
		if (typeof get === 'undefined' || typeof patch === 'undefined') {
			return;
		}

		const getAssoc = get.associatedDefinition;
		const patchAssoc = get.associatedDefinition;
		if (typeof getAssoc === 'undefined' || typeof patchAssoc === 'undefined' || getAssoc !== patchAssoc) {
			return;
		}

		return getAssoc;
	}

	public getResourceForPostOrShortcut(action: OpenApiKubernetesAction): Definition|undefined
	{
		const post = this.endpoints.find((endpoint: Endpoint) => endpoint.action === OpenApiKubernetesAction.Post);
		if (typeof post === 'undefined') {
			return;
		}

		const byName = this.factories.find((factory: EndpointsGroupFactory) => factory.methodName === 'byName');
		if (typeof byName === 'undefined') {
			return;
		}

		const method = byName.targetGroup.endpoints.find((endpoint) => endpoint.action === action);
		if (typeof method === 'undefined') {
			return;
		}

		const assoc = method.associatedDefinition;
		if (typeof assoc === 'undefined') {
			return;
		}

		const metadata = assoc.properties.find((property) => property.name === 'metadata');
		if (typeof metadata === 'undefined') {
			return;
		}

		return assoc;
	}

	public shouldGenerateExistsShortcut(): boolean
	{
		const get = this.endpoints.find((endpoint) => endpoint.action === OpenApiKubernetesAction.Get);
		if (typeof get === 'undefined') {
			return false;
		}

		return typeof get.associatedDefinition !== 'undefined';
	}
}
