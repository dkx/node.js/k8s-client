import * as R from 'ramda';
import * as path from 'path';

import {OpenApiDefinition, OpenApiDocument, OpenApiParameter, OpenApiPath} from './typings';
import {Definition} from './definition';
import {EndpointsGroup} from './endpoints-group';
import {EndpointParameter} from './endpoint-parameter';
import {Endpoint} from './endpoint';
import {EndpointsGroupFactory} from './endpoints-group-factory';
import {RootEndpointGroup} from './root-endpoint-group';


const URL_PARAM_REGEX = /^{([a-z]+)}$/;
const ENDPOINT_METHODS = ['get', 'post', 'put', 'patch', 'delete'];


export declare interface ApiVersionKindInfo
{
	apiVersion: string,
	kind: string,
}

export class Document
{
	public readonly definitions: Array<Definition>;

	public readonly endpointsGroups: Array<EndpointsGroup>;

	constructor(
		private readonly source: OpenApiDocument,
	) {
		this.definitions = this.mapDefinitions();
		this.endpointsGroups = this.mapEndpointsGroups();
	}

	public getDefinitionByName(name: string): Definition
	{
		const definition = this.definitions.find((definition) => definition.name === name);
		if (typeof definition === 'undefined') {
			throw new Error(`Document: definition "${name}" does not exists`);
		}

		return definition;
	}

	public getDefinitionByReference(ref: string): Definition
	{
		return this.getDefinitionByName(ref.substring(14));
	}

	public findDefinitionByApiVersionAndKind(apiVersion: string, kind: string): Definition|undefined
	{
		return this.definitions.find((definition) => definition.hasApiVersionAndKind(apiVersion, kind));
	}

	private mapDefinitions(): Array<Definition>
	{
		return R.pipe(
			R.mapObjIndexed((definition: OpenApiDefinition, name: string): Definition => {
				return new Definition(this, name, definition);
			}),
			R.values,
		)(this.source.definitions || {});
	}

	private mapEndpointsGroups(): Array<EndpointsGroup>
	{
		// create "real" endpoints
		const endpoints: Array<EndpointsGroup> = R.pipe(
			R.mapObjIndexed((path: OpenApiPath, url: string): EndpointsGroup => {
				const parameters = (path.parameters || []).map((parameter) => {
					return new EndpointParameter(parameter);
				});

				// google cloud endpoints are missing the {namespace} and {name} parameters
				url
					.split('/')
					.map((part) => URL_PARAM_REGEX.exec(part))
					.filter((match) => match !== null)
					.map((match) => match[1])
					.forEach((name) => {
						const parameter = parameters.find((parameter) => parameter.name === name && parameter.in === 'path');
						if (typeof parameter === 'undefined') {
							parameters.push(new EndpointParameter({
								name,
								in: 'path',
								type: 'string',
								required: true,
							}));
						}
					});

				const endpoints: Array<Endpoint> = [];

				ENDPOINT_METHODS.forEach((method) => {
					if (typeof path[method] !== 'undefined') {
						const endpointParameters = [
							...parameters,
							...(path[method].parameters || []).map((parameter: OpenApiParameter) => {
								return new EndpointParameter(parameter);
							}),
						];

						endpoints.push(new Endpoint(this, method, endpointParameters, path[method]));
					}
				});

				const urlParameters = url
					.split('/')
					.map((part) => URL_PARAM_REGEX.exec(part))
					.filter((match) => match !== null)
					.map((match) => match[1])
					.map((name) => {
						const parameter = parameters.find((parameter) => parameter.name === name && parameter.in === 'path');
						if (typeof parameter === 'undefined') {
							throw new Error(`Url parameter "${name}" from "${url}" is missing in the parameters list`);
						}

						return parameter;
					});

				const group = new EndpointsGroup(createEndpointsGroupFileName(url));

				group.url = url;
				group.endpoints = endpoints;
				group.parameters = parameters;
				group.urlParameters = urlParameters;

				const factoryParameterMatch = /\/{([a-z]+)}$/.exec(url);
				if (factoryParameterMatch !== null) {
					const factoryParameter = parameters.find((parameter) => parameter.name === factoryParameterMatch[1] && parameter.in === 'path');
					if (typeof factoryParameter === 'undefined') {
						throw new Error('Should not happen');
					}

					group.factoryParameter = factoryParameter;
				}

				return group;
			}),
			R.values,
		)(this.source.paths || {});

		// create all parents
		for (let i = endpoints.length - 1; i >= 0; i--) {
			let endpoint = endpoints[i];
			let url = endpoint.url;

			if (typeof url === 'undefined') {
				throw new Error('Should not happen');
			}

			let current = url
				.replace(/\/+$/, '')
				.replace(/^\/+/, '');

			//console.log('ROOT: ' + current);
			while ((current = path.dirname(current)) !== '.') {
				let fileName = createEndpointsGroupFileName(current);
				let existing: EndpointsGroup = endpoints.find((existing) => existing.fileName === fileName);
				if (typeof existing === 'undefined') {
					existing = new EndpointsGroup(fileName);
					endpoints.push(existing);
				}
				//console.log('CHILD: ' + current);

				let factoryParameterMatch = /\/{([a-z]+)}$/.exec(current);
				if (factoryParameterMatch !== null) {
					let factoryParameter = endpoint.parameters.find((parameter) => parameter.name === factoryParameterMatch[1] && parameter.in === 'path');
					if (typeof factoryParameter === 'undefined') {
						throw new Error('Should not happen');
					}

					existing.factoryParameter = factoryParameter;
				}

				let urlParametersRegex = /\/{([a-z]+)}/g;
				let urlParametersMatch: RegExpExecArray;
				while ((urlParametersMatch = urlParametersRegex.exec(current)) !== null) {
					let urlParameter = endpoint.parameters.find((parameter) => parameter.name === urlParametersMatch[1] && parameter.in === 'path');
					if (typeof urlParameter === 'undefined') {
						throw new Error('Should not happen');
					}

					//console.log(urlParameter.name);

					let existingUrlParameter = existing.urlParameters.find((parameter) => parameter.name === urlParameter.name && parameter.in === 'path');
					if (typeof existingUrlParameter === 'undefined') {
						existing.urlParameters.push(urlParameter);
					}
				}
			}
		}

		// create factories
		for (let endpoint of endpoints) {
			let parentFileName = path.dirname(endpoint.fileName) + '-api.ts';
			let parent = endpoints.find((parent) => parent.fileName === parentFileName);
			if (typeof parent !== 'undefined') {
				parent.factories.push(new EndpointsGroupFactory(endpoint));
			}
		}

		// create root
		const rootEndpoint = new RootEndpointGroup();
		rootEndpoint.factories = endpoints
			.filter((endpoint) => {
				return (endpoint.fileName.match(/\//g) || []).length === 1;
			})
			.map((endpoint) => {
				return new EndpointsGroupFactory(endpoint);
			});

		endpoints.push(rootEndpoint);

		return endpoints;
	}
}

function createEndpointsGroupFileName(url: string): string
{
	const parts = url
		.split('/')
		.filter((part) => part !== '')
		.map((part) => {
			const match = URL_PARAM_REGEX.exec(part);
			if (match !== null) {
				return `by-${match[1]}`;
			}

			return part;
		})
		.map((part) => part.replace(/\./g, '-'));

	return path.join('endpoint', ...parts) + '-api.ts';
}
