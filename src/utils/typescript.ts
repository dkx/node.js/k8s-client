import * as ts from 'typescript';
import * as R from 'ramda';

import {relativeImportPath} from './paths';


export declare interface ImportRequest
{
	targetFileName: string,
	names: Array<string>,
}

export declare interface NodeWithImportRequests<T extends ts.Node>
{
	imports: Array<ImportRequest>,
	node: T,
}

export function createNodeWithImportRequests<T extends ts.Node>(node: T, imports: Array<ImportRequest> = []): NodeWithImportRequests<T>
{
	return {
		imports,
		node,
	};
}

export function addJSDoc<T extends ts.Node>(node: T, content: Array<string>): T
{
	if (content.length === 0) {
		return node;
	}

	content = content
		.map((line) => {
			return line.replace(/\*\//g, '*' + String.fromCharCode(8203) + '/');
		})
		.map((line) => {
			return line.replace(/\n/gm, '\n * ');
		})
		.map((line) => {
			return ` * ${line}`;
		});

	content.unshift('*');
	content.push(' ');

	const text = content.join('\n');

	return ts.addSyntheticLeadingComment(node, ts.SyntaxKind.MultiLineCommentTrivia, text, true);
}

/**
 * Generates:
 * import {<...names>} from "<targetFileName>";
 */
export function createNamedImportsFromRequests(sourceFileName: string, imports: Array<ImportRequest>): Array<ts.ImportDeclaration>
{
	const unique: Array<ImportRequest> = [];

	imports.forEach((request) => {
		if (sourceFileName === request.targetFileName) {
			return;
		}

		const existing = unique.find((existing) => existing.targetFileName === request.targetFileName);
		if (typeof existing === 'undefined') {
			unique.push({
				targetFileName: request.targetFileName,
				names: [...request.names],
			});
		} else {
			existing.names = R.uniq([...existing.names, ...request.names]);
		}
	});

	return unique.map((request) => {
		const targetPath = request.targetFileName.startsWith('@') ?
			request.targetFileName :
			relativeImportPath(sourceFileName, request.targetFileName);

		return ts.createImportDeclaration(
			[],
			[],
			ts.createImportClause(
				undefined,
				ts.createNamedImports(request.names.map((name) => {
					return ts.createImportSpecifier(undefined, ts.createIdentifier(name));
				})),
			),
			ts.createStringLiteral(targetPath),
		);
	});
}

export function createStringConcatenation(nodes: Array<ts.Expression>): ts.Expression
{
	const optimized: Array<ts.Expression> = [];

	for (let i = 0; i < nodes.length; i++) {
		let node = nodes[i];

		if (ts.isStringLiteral(node) && node.text === '') {
			continue;
		}

		if (!ts.isStringLiteral(node) || optimized.length === 0) {
			optimized.push(node);
			continue;
		}

		let previous = optimized[optimized.length - 1];

		if (ts.isStringLiteral(node) && ts.isStringLiteral(previous)) {
			previous.text = previous.text + node.text;
		} else {
			optimized.push(node);
		}
	}

	if (optimized.length === 0) {
		throw new Error('Can not concatenate empty list of nodes');
	}

	if (optimized.length === 1) {
		return optimized[0];
	}

	return doCreateStringConcatenation(optimized);
}

function doCreateStringConcatenation(nodes: Array<ts.Expression>): ts.BinaryExpression
{
	const right = nodes.pop();
	const left = nodes.length === 1 ? nodes[0] : createStringConcatenation(nodes);

	return ts.createBinary(
		left,
		ts.createToken(ts.SyntaxKind.PlusToken),
		right,
	);
}
