export function firstLower(str: string): string
{
	return str.charAt(0).toLowerCase() + str.slice(1);
}

export function firstUpper(str: string): string
{
	return str.charAt(0).toUpperCase() + str.slice(1);
}

export function insertString(str: string, position: number, insert: string): string
{
	return str.slice(0, position) + insert + str.slice(position);
}

export function camelCaseToHyphens(str: string): string
{
	return str
		.replace(/^([A-Z]+)$/, (g) => g.toLowerCase())
		.replace(/^([A-Z]{2,})/, (g) => insertString(g.toLowerCase(), g.length - 1, '-'))
		.replace(/^([A-Z])/, (g) => g[0].toLowerCase())
		.replace(/([A-Z]{2,})/g, (g) => `-${g.toLowerCase()}`)
		.replace(/([A-Z])/g, (g) => `-${g[0].toLowerCase()}`)
}

export function hyphensToCamelCase(str: string): string
{
	return str.replace(/-([a-z])/g, (g) => g[1].toUpperCase());
}
