#!/usr/bin/env node

import * as path from 'path';
import * as yargs from 'yargs';

import {ApiGenerator, CodeWriter} from '../generators';
import {FileLoader, HttpLoader, Loader} from '../loaders';


function createLoader(argv: any): Loader
{
	if (typeof argv.file !== 'undefined') {
		const file: string = path.isAbsolute(argv.file) ?
			argv.file :
			path.join(process.cwd(), argv.file);

		return new FileLoader(file);
	}

	if (typeof argv.ver !== 'undefined') {
		return new HttpLoader(argv.ver);
	}

	throw new Error('Could not create specification loader');
}

yargs
	.command('gen', 'Generate client', (yargs) => {
		return yargs
			.option('file', {
				type: 'string',
				description: 'Load specification from file',
			})
			.option('ver', {
				type: 'string',
				description: 'Download specification for given version from github',
			})
			.option('out', {
				type: 'string',
				description: 'Output directory',
				default: path.join(__dirname, '..', '..', 'gen'),
			})
			.conflicts('file', 'ver')
			.check((argv) => {
				if (typeof argv.file === 'undefined' && typeof argv.ver === 'undefined') {
					throw new Error('Pass either --file or --ver option');
				}

				return true;
			});
	}, async (argv) => {
		const outputDir: string = path.isAbsolute(argv.out) ?
			argv.out :
			path.join(process.cwd(), argv.out);

		console.log('Loading specification...');
		const doc = await createLoader(argv).loadDocument();

		const writer = new CodeWriter();
		const generator = new ApiGenerator(writer, doc, outputDir);

		await generator.generateApi();
		console.log('done');
	})
	.showHelpOnFail(false, 'Specify --help for available options')
	.demandCommand()
	.argv;
