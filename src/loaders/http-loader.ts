import fetch from 'node-fetch';

import {Loader} from './loader';
import {Document} from '../openapi';


export class HttpLoader implements Loader
{
	constructor(
		private readonly version: string,
	) {}

	public async loadDocument(): Promise<Document>
	{
		const url = `https://raw.githubusercontent.com/kubernetes/kubernetes/release-${this.version}/api/openapi-spec/swagger.json`;
		const res = await fetch(url);
		const data = await res.json();

		return new Document(data);
	}
}
