import * as fs from 'fs-extra';

import {Loader} from './loader';
import {Document} from '../openapi';


export class FileLoader implements Loader
{
	constructor(
		private readonly file: string,
	) {}

	public async loadDocument(): Promise<Document>
	{
		const source = await fs.readFile(this.file, {encoding: 'utf8'});
		return new Document(JSON.parse(source));
	}
}
