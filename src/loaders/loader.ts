import {Document} from '../openapi';


export interface Loader
{
	loadDocument(): Promise<Document>;
}
