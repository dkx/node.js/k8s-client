# K8S Client

Kubernetes client in typescript/javascript.

## Installation

```bash
$ npm install --save @dkx/k8s-client
```

## Supported version

* 1.14
* 1.15
* 1.16
* 1.17

## Generate client

Because each kubernetes cluster can be different, you'll have to first generate the client. This makes it possible to use 
custom resources (read more [here](https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definitions/#publish-validation-schema-in-openapi-v2).

There are two ways how to generate a client. The preferred way is to generate it from the specification from your server. 
Simpler option is to generate it from specification on github ([swagger.json](https://github.com/kubernetes/kubernetes/blob/release-1.17/api/openapi-spec/swagger.json)).

**Generate from own specification:**

1. Download open-api specification from your cluster:

    Replace `<TOKEN>` with your cluster token and `<HOST>` with your cluster host name.

    ```bash
    $ curl --cacert ca.pem -H "Authorization: Bearer <TOKEN>" https://<HOST>/openapi/v2 -o spec.json
    ```

2. Generate client

    ```bash
    $ npx k8s-client gen --file spec.json
    ```

You can commit downloaded specification with your project. This will ensure that all your colleagues will end up with 
exactly the same client.

**Generate from version:**

1. Generate client

    ```bash
    $ npx k8s-client gen --ver 1.17
    ```

**Custom output directory:**

```bash
$ npx k8s-client gen --ver 1.17 --out ./k8s-client
```

The `--out` option can be used to generate own npm package for your organization and storing it in eg.
[Verdaccio](https://verdaccio.org/) or any other private npm registry.

**Add script to package.json:**

```json
{
    "scripts": {
        "k8s-client:gen": "k8s-client gen --file spec.json"
    }
}
```

and run it with:

```bash
$ npm run k8s-client:gen
```

## Create client

```typescript
import {Fetch, Client} from '@dkx/k8s-client';

const fetch = new Fetch('https://xxx.xxx.xxx.xxx', {
    token: getMyToken(),
    ca: getMyCertificate(),
});

const client = new Client(fetch);
```

## Create deployment

```typescript
import {Deployment} from '@dkx/k8s-client/resource/api/apps/v1/Deployment';

const deployment = new Deployment({
    metadata: {
        namespace: 'default',
        name: 'my-app',
    },
    spec: {
        selector: {
            matchLabels: {/* ... */},
        },
        template: {/* ... */},
    },
});
```

## Use API

```typescript
await client.apis.apps.v1.namespaces.byNamespace(deployment.metadata.namespace).deployments.post(deployment);
```

## Shortcuts

**exists:**

```typescript
await client.apis.apps.v1.namespaces.byNamespace(deployment.metadata.namespace).deployments.byName(deployment.metadata.name).exists();
```

**patchResource:**

```typescript
await client.apis.apps.v1.namespaces.byNamespace(deployment.metadata.namespace).deployments.patchResource(deployment);
```

**postOrPut:**

```typescript
await client.apis.apps.v1.namespaces.byNamespace(deployment.metadata.namespace).deployments.postOrPut(deployment);
```

**postOrPatch:**

```typescript
await client.apis.apps.v1.namespaces.byNamespace(deployment.metadata.namespace).deployments.postOrPatch(deployment);
```

## Logging

**Predefined loggers:**

* `StdOutLogger`: Simple logs with console.log (default)
* `NullLogger`: Logs nowhere

**Using `StdOutLogger`:**

```typescript
import {Fetch, Client, StdOutLogger} from '@dkx/k8s-client';

const fetch = new Fetch('https://xxx.xxx.xxx.xxx');
const logger = new StdOutLogger();
const client = new Client(fetch, logger);
```

**Custom logger:**

Check out the [StdOutLogger](https://gitlab.com/dkx/node.js/k8s-client/blob/master/static/logging/std-out-logger.ts)
and [Logger](https://gitlab.com/dkx/node.js/k8s-client/blob/master/static/logging/logger.ts) interface source codes.
