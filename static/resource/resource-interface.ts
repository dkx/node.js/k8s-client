import {ObjectMeta} from './io/k8s/apimachinery/pkg/apis/meta/v1/object-meta';


export interface Resource
{
	apiVersion?: string,
	kind?: string,
	metadata?: ObjectMeta,
	toJson(): any;
}
