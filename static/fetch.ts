import fetch, {HeadersInit, RequestInit} from 'node-fetch';
import {Agent, AgentOptions} from 'https';

import {
	ApiResponseError,
	ApiResponseForbiddenError,
	ApiResponseNotFoundError,
	ApiUnsupportedResponseError
} from './error';


export declare interface FetchOptions
{
	token?: string,
	ca?: string,
}

export class Fetch
{
	private readonly headers: HeadersInit = {};

	private readonly agent: Agent;

	constructor(
		private host: string,
		options: FetchOptions = {},
	) {
		if (typeof options.token !== 'undefined') {
			this.headers['Authorization'] = `Bearer ${options.token}`;
		}

		const agentOptions: AgentOptions = {};

		if (typeof options.ca !== 'undefined') {
			agentOptions.ca = options.ca;
		}

		this.agent = new Agent(agentOptions);
	}

	public async request(method: string, url: string, body?: any): Promise<any>
	{
		const init: RequestInit = {
			method,
			agent: this.agent,
			headers: {
				...this.headers,
				'Content-Type': method.toUpperCase() === 'PATCH' ? 'application/json-patch+json' : 'application/json',
			},
		};

		if (typeof body !== 'undefined') {
			if (typeof body !== 'string') {
				body = JSON.stringify(body);
			}

			init.body = body;
		}

		const res = await fetch(`${this.host}${url}`, init);

		if (res.status === 403) {
			throw new ApiResponseForbiddenError(res, `403: Not allowed`);
		}

		if (res.status === 404) {
			throw new ApiResponseNotFoundError(res, `404: Not found`);
		}

		const contentType = res.headers.get('content-type');
		if (contentType !== 'application/json') {
			throw new ApiUnsupportedResponseError(res, `Unsupported response type "${contentType}"`);
		}

		const json = await res.json();

		if ((res.status + '').charAt(0) !== '2') {
			const message = typeof json.message === 'string' ? json.message : res.statusText;
			throw new ApiResponseError(res, `${res.status}: ${message}`);
		}

		return json;
	}
}
