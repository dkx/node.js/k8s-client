import {Response} from 'node-fetch';


export class ApiUnsupportedResponseError extends Error
{
	constructor(
		public readonly response: Response,
		message: string,
	) {
		super(message);
		Object.setPrototypeOf(this, ApiUnsupportedResponseError);
	}
}
