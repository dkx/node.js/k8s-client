import {Response} from 'node-fetch';


export class ApiResponseError extends Error
{
	constructor(
		public readonly response: Response,
		message: string,
	) {
		super(message);
		Object.setPrototypeOf(this, ApiResponseError.prototype);
	}
}
