import {Response} from 'node-fetch';

import {ApiResponseError} from './api-response-error';


export class ApiResponseForbiddenError extends ApiResponseError
{
	constructor(response: Response, message: string)
	{
		super(response, message);
		Object.setPrototypeOf(this, ApiResponseForbiddenError.prototype);
	}
}
