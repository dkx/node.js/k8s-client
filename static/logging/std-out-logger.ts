import {LoggedData, Logger, UrlParameters} from './logger';


export class StdOutLogger implements Logger
{
	public async get(url: string, parameters: UrlParameters, data: LoggedData): Promise<void> {}

	public post(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>
	{
		return this.logCall('Created', url, data);
	}

	public put(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>
	{
		return this.logCall('Replaced', url, data);
	}

	public delete(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>
	{
		return this.logCall('Deleted', url, data);
	}

	public async patch(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>
	{
		const prefix = Array.isArray(data.body) && data.body.length === 0 ?
			'Unchanged' :
			'Updated';

		await this.logCall(prefix, url, data);

		if (Array.isArray(data.body)) {
			data.body.forEach((operation) => {
				console.log(`  - ${operation.op} ${operation.path}`);
			});
		}
	}

	private async logCall(prefix: string, url: string, data: LoggedData): Promise<void>
	{
		console.log(this.createMessage(prefix, url, data));
	}

	private createMessage(prefix: string, url: string, data: LoggedData): string
	{
		const typeParts = [];
		if (typeof data.apiVersionKind !== 'undefined') {
			typeParts.push(data.apiVersionKind.apiVersion);
			typeParts.push(data.apiVersionKind.kind);
		}

		const nameParts = [];
		if (typeof data.resource !== 'undefined' && typeof data.resource.metadata !== 'undefined') {
			if (typeof data.resource.metadata.namespace !== 'undefined') {
				nameParts.push(data.resource.metadata.namespace);
			}

			if (typeof data.resource.metadata.name !== 'undefined') {
				nameParts.push(data.resource.metadata.name);
			}
		}

		if (typeParts.length > 1 && nameParts.length > 1) {
			return `${prefix} ${typeParts.join('/')} ${nameParts.join('/')}`;
		}

		if (typeParts.length > 1) {
			return `${prefix} ${typeParts.join('/')}`;
		}

		if (nameParts.length > 1) {
			return `${prefix} ${nameParts.join('/')}`;
		}

		return `${prefix} ${url}`;
	}
}
