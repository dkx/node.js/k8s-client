import {Resource} from '../resource';


export declare interface LoggedData
{
	result: any,
	endpoint: {
		method: string,
		urlPattern: string,
	},
	body?: any,
	resource?: Resource,
	definition?: {
		fullName: string,
		className: string,
	},
	apiVersionKind?: {
		apiVersion: string,
		kind: string,
	},
}

export declare interface UrlParameters
{
	[name: string]: string,
}


export interface Logger
{
	get(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>;

	post(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>;

	put(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>;

	patch(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>;

	delete(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>;
}
